package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class TabStudentLeave extends ListFragment {

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private DbHelper dbHelper;
    private SharedPreferences spLogin;
    private OnFragmentInteractionListener mListener;
    private AsyncGetLeaves asyncGetLeaves;
    private ProgressBar pbLoading;
    private LinearLayout llNothingP;
    private TextView tvNothingP;

    public TabStudentLeave() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TabStudentLeave newInstance(String param1, String param2) {
        TabStudentLeave fragment = new TabStudentLeave();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_student_leave, container, false);
        pbLoading = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        tvNothingP = (TextView)view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("No leave history yet.");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        dbHelper = new DbHelper(getContext());
        pbLoading.setVisibility(View.GONE);
        try{
            if((asyncGetLeaves!=null)&&(asyncGetLeaves.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetLeaves .cancel(true);
            asyncGetLeaves = new AsyncGetLeaves();
            asyncGetLeaves.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class LeaveItem {
        String _leave_purpose;
        String _date_from;
        String _date_to;
        String _applied_by;
        String _applied_date;
        String _student_id;
        String _status;
        String _approved_by;
        String _id;
        String _student_name;
        String _class_name;
        String _dates;
        String _day;
        String _month;
    }

    private class AsyncGetLeaves extends AsyncTask<Void, Void, String> {

        String _requestURL;
        String _staff_id;
        String _academic_id;
        ArrayList<LeaveItem> list;
        LeaveItem leaveItem;

        @Override
        protected void onPreExecute() {
            list = new ArrayList<>();
            pbLoading.setVisibility(View.VISIBLE);
            _staff_id = spLogin.getString(Config.ID, null);
            _academic_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            _requestURL = Config.URL + Config.URL_STUDENT_LEAVE + "?" + Config.STAFF_ID + "=" + _staff_id +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + _academic_id;
            System.out.println("-->request URL "+ _requestURL);
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonLeaveCat = new JSONArray(_response);
                for (int i = 0; i < jsonLeaveCat.length(); i++) {
                    JSONObject jsonCat = jsonLeaveCat.getJSONObject(i);
                    for (int j = 0; j < jsonCat.getJSONArray("leavemodel").length(); j++) {
                        JSONObject jsonLeave = jsonCat.getJSONArray("leavemodel").getJSONObject(j);
                        leaveItem = new LeaveItem();
                        leaveItem._applied_by = staffControl.getNullStr(jsonLeave.getString("ApplieBy"));
                        leaveItem._applied_date = staffControl.getNullStr(jsonLeave.getString("AppliedDate"));
                        leaveItem._approved_by = staffControl.getNullStr(jsonLeave.getString("ApprovedBy"));
                        leaveItem._class_name = staffControl.getNullStr(jsonLeave.getString("ClassName"));
                        leaveItem._date_from = staffControl.getNullStr(jsonLeave.getString("Date_From"));
                        leaveItem._date_to = staffControl.getNullStr(jsonLeave.getString("Date_To"));
                        leaveItem._id = staffControl.getNullStr(jsonLeave.getString("ID"));
                        leaveItem._leave_purpose = staffControl.getNullStr(jsonLeave.getString("LeavePurpose"));
                        leaveItem._status = staffControl.getNullStr(jsonLeave.getString("Staus"));
                        leaveItem._student_id = staffControl.getNullStr(jsonLeave.getString("StudentID"));
                        leaveItem._student_name = staffControl.getNullStr(jsonLeave.getString("Studentname"));
                        leaveItem._applied_date = String.valueOf(staffControl.getDateByString(leaveItem._applied_date).get(Calendar.DATE)) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._applied_date).get(Calendar.MONTH) + 1) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._applied_date).get(Calendar.YEAR));
                        leaveItem._dates = String.valueOf(staffControl.getDateByString(leaveItem._date_from).get(Calendar.DATE)) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._date_from).get(Calendar.MONTH) + 1) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._date_from).get(Calendar.YEAR)) + " - "
                                + String.valueOf(staffControl.getDateByString(leaveItem._date_to).get(Calendar.DATE)) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._date_to).get(Calendar.MONTH) + 1) + "/"
                                + String.valueOf(staffControl.getDateByString(leaveItem._date_to).get(Calendar.YEAR));
                        leaveItem._day = String.valueOf(leaveItem._applied_date.substring(0, leaveItem._applied_date.indexOf("/")));
                        leaveItem._month = staffControl.getMonthById(Integer.valueOf(leaveItem._applied_date.substring(leaveItem._applied_date.indexOf("/") + 1, leaveItem._applied_date.lastIndexOf("/"))) - 1);
                        list.add(leaveItem);
                    }
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initLeaveList(list);
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbLoading.setVisibility(View.GONE);
        }
    }

    private class AdapterLeaveList extends ArrayAdapter<LeaveItem> implements View.OnClickListener {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterLeaveList(@NonNull Context context, @LayoutRes int resource, @NonNull List<LeaveItem> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_student_leave, null);
                viewHolder = new ViewHolder();
                viewHolder.tvAppliedBy = (TextView) convertView.findViewById(R.id.tvStudLListAppliedBy);
                viewHolder.tvDates = (TextView) convertView.findViewById(R.id.tvStudLListDates);
                viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tvStudLListDay);
                viewHolder.tvMonth = (TextView) convertView.findViewById(R.id.tvStudLListMonth);
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvStudLListName);
                viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvStudLListStatus);
                viewHolder.tvReason = (TextView) convertView.findViewById(R.id.tvStudLListReason);
                viewHolder.btnAccept = (Button) convertView.findViewById(R.id.btnStudLeaveApprove);
                viewHolder.btnReject = (Button) convertView.findViewById(R.id.btnStudLeaveReject);
                viewHolder.llButtons = (LinearLayout) convertView.findViewById(R.id.llStudLeaveButtons);
                viewHolder.pbLoading = (ProgressBar) convertView.findViewById(R.id.pbStudListLoad);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            LeaveItem leaveItem = getItem(position);

            viewHolder.tvStatus.setText(leaveItem._status);
            viewHolder.tvName.setText(leaveItem._student_name);
            viewHolder.tvMonth.setText(leaveItem._month);
            viewHolder.tvDates.setText(leaveItem._dates);
            viewHolder.tvDay.setText(leaveItem._day);
            viewHolder.tvReason.setText(leaveItem._leave_purpose);
            viewHolder.tvAppliedBy.setText(leaveItem._applied_by);
            viewHolder.position = position;
            if (leaveItem._status.equals("Applied")) {
                viewHolder.llButtons.setVisibility(View.VISIBLE);
                viewHolder.btnReject.setTag(viewHolder);
                viewHolder.btnAccept.setTag(viewHolder);
                viewHolder.btnAccept.setOnClickListener(this);
                viewHolder.btnReject.setOnClickListener(this);
            } else {
                viewHolder.llButtons.setVisibility(View.GONE);
            }

            return convertView;
        }

        @Override
        public void onClick(View v) {
            String _staff_id = spLogin.getString(Config.ID, null);
            final ViewHolder viewHolder = (ViewHolder) v.getTag();
            LeaveItem leaveItem = getItem(viewHolder.position);
            if (v.getId() == viewHolder.btnAccept.getId()) {
                new AsyncTask<String, Void, String>() {

                    @Override
                    protected void onPreExecute() {
                        viewHolder.pbLoading.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String _leave_id = params[0];
                        String _leave_status = params[1];
                        String _leave_staff_id = params[2];
                        String _response = dbControl.sendStudLeaveApprove(_leave_id, _leave_status, _leave_staff_id);
                        return _response;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        if (s.equals(Config.SUCCESS)) {
                            Toast.makeText(getContext(), "Leave Approved Successfully.", Toast.LENGTH_SHORT).show();
                            viewHolder.llButtons.setVisibility(View.GONE);
                            viewHolder.tvStatus.setText("Approved");
                        } else {
                            Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                        }
                        viewHolder.pbLoading.setVisibility(View.INVISIBLE);
                    }
                }.execute(leaveItem._id, "Approved", _staff_id);
            } else if (v.getId() == viewHolder.btnReject.getId()) {
                new AsyncTask<String, Void, String>() {

                    @Override
                    protected void onPreExecute() {
                        viewHolder.pbLoading.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String _leave_id = params[0];
                        String _leave_status = params[1];
                        String _leave_staff_id = params[2];
                        String _response = dbControl.sendStudLeaveApprove(_leave_id, _leave_status, _leave_staff_id);
                        return _response;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        if (s.equals(Config.SUCCESS)) {
                            viewHolder.llButtons.setVisibility(View.GONE);
                            viewHolder.tvStatus.setText("Rejected");
                            Toast.makeText(getContext(), "Leave Rejected Successfully.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                        }
                        viewHolder.pbLoading.setVisibility(View.INVISIBLE);
                    }
                }.execute(leaveItem._id, "Rejected", _staff_id);

            }
        }

        private class ViewHolder {
            TextView tvName;
            TextView tvDates;
            TextView tvMonth;
            TextView tvDay;
            TextView tvStatus;
            TextView tvAppliedBy;
            TextView tvReason;
            Button btnAccept;
            Button btnReject;
            LinearLayout llButtons;
            ProgressBar pbLoading;
            int position;
        }
    }

    private void initLeaveList(ArrayList<LeaveItem> list) {
        if ((list != null) && (list.size() > 0)) {
            setListAdapter(new AdapterLeaveList(getContext(), 0, list));
           dbHelper.updateNotif(Config.NOTIF_LEAVE,list.size(),0);
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if((asyncGetLeaves!=null)&&(asyncGetLeaves.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetLeaves.cancel(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
