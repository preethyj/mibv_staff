package com.eloitinnovations.edisappstaff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by user on 12-04-2017.
 */

public class PagerChatsStaff extends FragmentStatePagerAdapter {

    int tabCount;
    ArrayList<ItemStaffMessages> listOutbox;
    ArrayList<ItemStaffMessages> listInbox;
    public PagerChatsStaff(FragmentManager fm, int tabCount,ArrayList<ItemStaffMessages> listOutbox,ArrayList<ItemStaffMessages> listInbox) {
        super(fm);
        this.tabCount = tabCount;
        this.listInbox = listInbox;
        this.listOutbox = listOutbox;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){

            case 1:
                return new ChatsStaffOutBoxFragment().newInstance(this.listOutbox);
            case 2:
                return new ContactsStaffFragment();
            default:
            case 0:
                return new ChatsStaffFragments().newInstance(this.listInbox);
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
