package com.eloitinnovations.edisappstaff;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 18-05-2017.
 */

public class ItemTimeTable implements Parcelable {
    String _period_name;
    String _start_time;
    String _end_time;
    String _staff_name;
    String _subject;
    String _class_name;

    ItemTimeTable(){

    }
    protected ItemTimeTable(Parcel in) {
        _period_name = in.readString();
        _start_time = in.readString();
        _end_time = in.readString();
        _staff_name = in.readString();
        _subject = in.readString();
        _class_name = in.readString();
    }

    public static final Creator<ItemTimeTable> CREATOR = new Creator<ItemTimeTable>() {
        @Override
        public ItemTimeTable createFromParcel(Parcel in) {
            return new ItemTimeTable(in);
        }

        @Override
        public ItemTimeTable[] newArray(int size) {
            return new ItemTimeTable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_period_name);
        dest.writeString(_start_time);
        dest.writeString(_end_time);
        dest.writeString(_staff_name);
        dest.writeString(_subject);
        dest.writeString(_class_name);
    }
}
