package com.eloitinnovations.edisappstaff;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class TabEventsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private DbControl dbControl;
    private StaffControl staffControl;
    private SharedPreferences spLogin;
    private DbHelper dbHelper;


    private ProgressBar pbTb;
    private ListView lvEvents;
    private LinearLayout llNothingP;
    private ArrayList<EachRow> list = new ArrayList<>();
    private Dialog dialog;
    private AsyncNewsAndEvents asyncNewsAndEvents;

    public TabEventsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TabEventsFragment newInstance() {
        TabEventsFragment fragment = new TabEventsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_events, container, false);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        pbTb = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        lvEvents = (ListView) view.findViewById(R.id.lvEventsList);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        dbControl = new DbControl();
        staffControl = new StaffControl();
        dbHelper = new DbHelper(getContext());
        pbTb.setVisibility(View.GONE);
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lvEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EachRow row = (EachRow) parent.getItemAtPosition(position);
                dialog.setContentView(R.layout.dialog_news);
                TextView tvDate = (TextView) dialog.findViewById(R.id.tvNewsDialogDate);
                TextView tvContent = (TextView) dialog.findViewById(R.id.tvNewsDialogContent);
                TextView tvNewsTitle = (TextView) dialog.findViewById(R.id.tvNewsDialogTitle);
                ((TextView)dialog.findViewById(R.id.tvDialogNewsEvent)).setText("Event");
                tvContent.setText(row._content);
                tvDate.setText(row._day + " " + row._month);
                tvNewsTitle.setVisibility(View.GONE);
                tvContent.setMovementMethod(new ScrollingMovementMethod());
                tvContent.setText(row._content);
                dialog.show();
            }
        });
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        if ((asyncNewsAndEvents != null) && (asyncNewsAndEvents.getStatus() == AsyncTask.Status.RUNNING))
            asyncNewsAndEvents.cancel(true);
        asyncNewsAndEvents = new AsyncNewsAndEvents();
        asyncNewsAndEvents.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_News_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class EachRow {
        String _date;
        String _content;
        String _id;
        String _title;
        String _month;
        String _day;
        String _type;
    }

    class AsyncNewsAndEvents extends AsyncTask<Void, Void, String> {

        String requestURL;
        EachRow row;
        String _created_date;

        @Override
        protected void onPreExecute() {
            pbTb.setVisibility(View.VISIBLE);
            requestURL = Config.URL + Config.URL_GET_ALL_NEWS_AND_EVENTS + Config.ACADEMIC_YEAR_ID + "="
                    + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            list.clear();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response = "ERROR";
            try {
                response = dbControl.sendGetRequest(requestURL);
                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    row = new EachRow();
                    if((jsonArray.getJSONObject(i).getString(Config.TYPE).equals("EVENTS"))) {
                        row._type = jsonArray.getJSONObject(i).getString(Config.TYPE);
                        row._date = jsonArray.getJSONObject(i).getString(Config.DATE);
                        row._content = jsonArray.getJSONObject(i).getString(Config.NEWS);
                        row._id = jsonArray.getJSONObject(i).getString(Config.ID);
                        row._day= row._date .substring(4);
                        row._month =  row._date .substring(0, 3);
                        if (row._content.length() > 50) {
                            row._content = row._content.substring(0, 45) + " ...";
                        }
                        list.add(row);
                    }
                }
                return response;
            } catch (JSONException e) {
                e.printStackTrace();
                return "ERROR";
            } catch (Exception e) {
                e.printStackTrace();
                return "ERROR";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.isEmpty() || s.equals("ERROR") || s.equals("null")) {

            } else {
                initListItems();
            }
            pbTb.setVisibility(View.GONE);
        }
    }


    private class AdapterNews extends ArrayAdapter<EachRow> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterNews(Context context, int resource, ArrayList<EachRow> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_news, null);
                viewHolder = new ViewHolder();
                viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tvNewsListContent);
                viewHolder.tvMonthName = (TextView) convertView.findViewById(R.id.tvNewsMonthName);
                viewHolder.tvMonthNum = (TextView) convertView.findViewById(R.id.tvNewsMonthNum);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            EachRow row = getItem(position);
            String _conttent = "";
            try {
                if (row._content.length() > 50) {
                    _conttent = row._content.substring(0, 45) + " ...";
                }else
                {
                    _conttent = row._content;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            viewHolder.tvContent.setText(_conttent);
            viewHolder.tvMonthName.setText(row._month);
            viewHolder.tvMonthNum.setText(row._day);
            return convertView;
        }

        @Nullable
        @Override
        public EachRow getItem(int position) {
            return list.get(position);
        }

        class ViewHolder {
            TextView tvMonthName;
            TextView tvMonthNum;
            TextView tvContent;
            TextView tvTitle;
        }
    }

    private class AsyncGetByid extends AsyncTask<String, Void, String> {

        TextView tvDate;
        TextView tvContent;
        TextView tvTitle;

        String _date;
        String _title;
        String _content;

        @Override
        protected void onPreExecute() {
            pbTb.setVisibility(View.VISIBLE);
            dialog.setContentView(R.layout.dialog_news);
            tvDate = (TextView) dialog.findViewById(R.id.tvNewsDialogDate);
            tvContent = (TextView) dialog.findViewById(R.id.tvNewsDialogContent);
            tvTitle = (TextView) dialog.findViewById(R.id.tvNewsDialogTitle);
        }

        @Override
        protected String doInBackground(String... url) {
            String response = "ERROR";
            try {
                response = dbControl.sendGetRequest(url[0]);
                JSONObject jsonObject = new JSONObject(response);

            } catch (JSONException e) {
                e.printStackTrace();
                return "ERROR";
            } catch (Exception e) {
                e.printStackTrace();
                return "ERROR";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("ERROR") || s.isEmpty()) {

            } else {
                tvContent.setText(staffControl.getNullStr(_content));
                tvDate.setText(staffControl.getNullStr(_date));
                tvTitle.setText(staffControl.getNullStr(_title));
                dialog.show();
            }
            pbTb.setVisibility(View.GONE);
        }
    }

    private void initListItems() {
        if ((list != null) && (list.size() > 0)) {
            lvEvents.setAdapter(new AdapterNews(getContext(), 0, list));
            dbHelper.updateNotif(Config.NOTIF_NEWS, list.size(), 0);
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncNewsAndEvents != null) && (asyncNewsAndEvents.getStatus() == AsyncTask.Status.RUNNING))
                asyncNewsAndEvents.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
