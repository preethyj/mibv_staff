package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnnouncementDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnnouncementDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnnouncementDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private ArrayList<String> mParam5;
    TextView tvTitle,tvFAnnouncDate,tvFAnnouncName,tvFAnnouncContent;
    Button btnViewFiles;


    private OnFragmentInteractionListener mListener;

    public AnnouncementDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param _staff
     * @param _content

     * @param urlList
     * @return A new instance of fragment ViewFilesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AnnouncementDetailFragment newInstance(String _staff, String _content, String _day, String _month, ArrayList<String> urlList) {
        AnnouncementDetailFragment fragment = new AnnouncementDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, _staff);
        args.putString(ARG_PARAM2, _content);
        args.putString(ARG_PARAM3, _day);
        args.putString(ARG_PARAM4, _month);
        args.putStringArrayList(ARG_PARAM5, urlList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getStringArrayList(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_announcement_details, container, false);
         tvFAnnouncDate = (TextView) view.findViewById(R.id.tvFAnnouncDate);
         tvFAnnouncName = (TextView) view.findViewById(R.id.tvFAnnouncName);
         tvFAnnouncContent = (TextView) view.findViewById(R.id.tvFAnnouncContent);
         btnViewFiles = (Button) view.findViewById(R.id.btnViewFiles);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        tvTitle.setText("Announcement");

        tvFAnnouncDate.setText(mParam3+" "+mParam4);
        tvFAnnouncName.setText(mParam1);
        tvFAnnouncContent.setText(mParam2);
        if(mParam5.isEmpty())
        {
            btnViewFiles.setVisibility(View.GONE);
        }else
        {
            btnViewFiles.setVisibility(View.VISIBLE);
            btnViewFiles.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                            ViewFilesFragment.newInstance(mParam5)).addToBackStack("TAG_ANNOUNCEMENT").commit();
                }
            });
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
