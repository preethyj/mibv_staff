package com.eloitinnovations.edisappstaff;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by user on 17-04-2017.
 */

public class DbHelper extends SQLiteOpenHelper {


    public DbHelper(Context context) {
        super(context,Config.DATABASE_NAME,null, Config.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+Config.TABLE_NOTIF+" ( id INTEGER PRIMARY KEY AUTOINCREMENT, "+Config.NOTIF_COUNT
                +" INTEGER, "+Config.NOTIF_SHOW_NOTIF+" INTEGER )");
        ContentValues contentValues = new ContentValues();
        contentValues.put(Config.NOTIF_COUNT,0);
        contentValues.put(Config.NOTIF_SHOW_NOTIF,0);
        for(int i=0;i<6;i++){
            db.insert(Config.TABLE_NOTIF,null,contentValues);
        }
        contentValues.clear();
    }


    public boolean updateNotif(int row,int count,int isCheck){
        boolean isInsert=false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Config.NOTIF_COUNT,count);
        contentValues.put(Config.NOTIF_SHOW_NOTIF,isCheck);
        db.update(Config.TABLE_NOTIF,contentValues,"id = ?",new String[]{String.valueOf(row)});

        return isInsert;
    }

    public void loginDeleteTables(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM "+Config.TABLE_NOTIF);
    }

    public int[] getNotifVal(int row){
        int[] col = new int[2];
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Config.TABLE_NOTIF,new String[]{Config.NOTIF_COUNT,Config.NOTIF_SHOW_NOTIF}
                ,"id = ?",new String[]{String.valueOf(row)},null,null,null);
        cursor.moveToFirst();
        if(cursor != null && cursor.moveToFirst()) {
            col[0] = cursor.getInt(cursor.getColumnIndex(Config.NOTIF_COUNT));
            col[1] = cursor.getInt(cursor.getColumnIndex(Config.NOTIF_SHOW_NOTIF));
            cursor.close();
        }
        return col;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+Config.TABLE_NOTIF);
    }

}
