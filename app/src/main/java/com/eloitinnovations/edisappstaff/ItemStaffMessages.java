package com.eloitinnovations.edisappstaff;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 25-05-2017.
 */

public class ItemStaffMessages implements Parcelable {
    String _staff_name;
    String _subject;
    String _content;
    String _date;

    protected ItemStaffMessages(Parcel in) {
        _staff_name = in.readString();
        _subject = in.readString();
        _content = in.readString();
        _date = in.readString();
    }

    public static final Creator<ItemStaffMessages> CREATOR = new Creator<ItemStaffMessages>() {
        @Override
        public ItemStaffMessages createFromParcel(Parcel in) {
            return new ItemStaffMessages(in);
        }

        @Override
        public ItemStaffMessages[] newArray(int size) {
            return new ItemStaffMessages[size];
        }
    };

    public ItemStaffMessages() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_staff_name);
        dest.writeString(_subject);
        dest.writeString(_content);
        dest.writeString(_date);
    }
}
