package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ChatsStaffOutBoxFragment extends ListFragment {

    private OnFragmentInteractionListener mListener;
    private ArrayList<ItemStaffMessages> listOutbox;
    private LinearLayout llNothingP;
    private TextView tvNothingP;

    public ChatsStaffOutBoxFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ChatsStaffOutBoxFragment newInstance(ArrayList<ItemStaffMessages> listOutbox) {
        ChatsStaffOutBoxFragment fragment = new ChatsStaffOutBoxFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("STAFF_OUTBOX",listOutbox);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.listOutbox = getArguments().getParcelableArrayList("STAFF_OUTBOX");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_staff_out_box, container, false);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        tvNothingP = (TextView) view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("No notification yet.");
        return  view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        llNothingP.setVisibility(View.GONE);
        if((listOutbox!=null)&&(listOutbox.size()>0)){
            setListAdapter(new AdapterOutbox(getContext(),0,listOutbox));
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ItemStaffMessages itemStaffMessages = (ItemStaffMessages) parent.getItemAtPosition(position);
                    Intent intent = new Intent(getActivity(),StaffViewMessageActivity.class);
                    intent.putExtra("TO_TV","To: ");
                    intent.putExtra("TO",itemStaffMessages._staff_name);
                    intent.putExtra("SUBJECT",itemStaffMessages._subject);
                    intent.putExtra("DATE",itemStaffMessages._date);
                    intent.putExtra("MSG",itemStaffMessages._content);
                    startActivity(intent);
                }
            });
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private class AdapterOutbox extends ArrayAdapter<ItemStaffMessages> {
        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterOutbox(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemStaffMessages> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                convertView = inflater.inflate(R.layout.list_staff_chats,null);
                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvStaffChatListName);
                viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvStaffChatListDate);
                viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvStaffChatListMsg);
                viewHolder.tvSubject = (TextView) convertView.findViewById(R.id.tvStaffChatListSubject);
                convertView.setTag(viewHolder);
            }
            if((position%2)==0){
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            }else {
                convertView.setBackgroundColor(Color.parseColor("#fcfcfc"));
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemStaffMessages itemStaffMessages = getItem(position);
            viewHolder.tvName.setText(itemStaffMessages._staff_name);
            viewHolder.tvDate.setText(itemStaffMessages._date);
            viewHolder.tvMsg.setText(itemStaffMessages._content);
            viewHolder.tvSubject.setText(itemStaffMessages._subject);
            return convertView;
        }

        private class ViewHolder{
            TextView tvName,tvDate,tvSubject,tvMsg;
        }
    }

}
