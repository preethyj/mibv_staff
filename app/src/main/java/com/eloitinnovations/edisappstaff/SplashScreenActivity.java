package com.eloitinnovations.edisappstaff;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;


public class SplashScreenActivity extends AppCompatActivity {
    private SharedPreferences spLogin;
    private Activity activity;
    private Handler handler;
    String currentVersion;
    private AsyncUpdate asyncUpdate;
    private LinearLayout llLogoBlue;
    private LinearLayout llUpdate;
    private ProgressBar pbLoading;
    private Button btnUpdate;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        llLogoBlue = (LinearLayout) findViewById(R.id.llSCLogoBlue);
        llUpdate = (LinearLayout) findViewById(R.id.rlSPUpdate);
        pbLoading = (ProgressBar) findViewById(R.id.pbFragmentSub);
        btnUpdate = (Button) findViewById(R.id.btnSpUpdate);
        handler = new Handler();
        activity = new LoginActivity();

        intent = new Intent();

        handler = new Handler();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            currentVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if ((asyncUpdate != null) && (asyncUpdate.getStatus().equals(AsyncTask.Status.RUNNING)))
                    asyncUpdate.cancel(true);
                asyncUpdate = new AsyncUpdate();
                asyncUpdate.execute();
            }
        }, 2000);
    }

    private class AsyncUpdate extends AsyncTask<Void, Void, String> {

        boolean isUpdate = false;

        @Override
        protected void onPreExecute() {
            pbLoading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String returnRes = Config.ERROR;
            try {
                String response = new DbControl().sendGetRequest(Config.BASE_URL + "/APIs/MobAppConf/getConfigByName/?configName=AppVersion");
                JSONObject jsonResult = new JSONObject(response);
                if (jsonResult.getBoolean("status")) {
                    JSONObject jsonAppVersion = jsonResult.getJSONObject("message");
                    String _app_version = jsonAppVersion.getString("StaffApp");
                    if (_app_version != null && !_app_version.isEmpty())
                        returnRes = _app_version;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnRes;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s != null && !s.isEmpty() && !s.equals(Config.ERROR) && currentVersion!= null && !currentVersion.isEmpty()) {
                    if (!s.equals(currentVersion))
                        isUpdate = true;
                }
                if (!isUpdate) {
                    nextToLogin();
                } else {
                    llLogoBlue.setVisibility(View.GONE);
                    llUpdate.setVisibility(View.VISIBLE);
                }
                pbLoading.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                nextToLogin();
            }
        }
    }

    private void nextToLogin() {
        try {
            spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
            if (spLogin.getAll().size() == 16) {
                activity = new HomeActivity();
            }
            startActivity(new Intent(SplashScreenActivity.this, activity.getClass()));
            finish();
        } catch (Exception e) {
            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncUpdate != null) && (asyncUpdate.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncUpdate.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

    }
}
