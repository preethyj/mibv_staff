package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ChatsParentFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private DbHelper dbHelper;
    private SharedPreferences spLogin;

    private ProgressBar pbTitle;
    private OnFragmentInteractionListener mListener;
    private AsyncgetChats asyncgetChats;
    private LinearLayout llNothingP;
    private TextView tvNothingP;

    public ChatsParentFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ChatsParentFragment newInstance() {
        ChatsParentFragment fragment = new ChatsParentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_parent, container, false);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        tvNothingP = (TextView) view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("No notification yet.");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        llNothingP.setVisibility(View.GONE);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        dbHelper = new DbHelper(getContext());
        try {
            if ((asyncgetChats != null) && (asyncgetChats.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncgetChats.cancel(true);
            asyncgetChats = new AsyncgetChats();
            asyncgetChats.execute(spLogin.getString(Config.ID, null), spLogin.getString(Config.ACADEMIC_YEAR_ID, null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        getListView().setOnItemClickListener(this);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ItemMessages itemMessages = (ItemMessages) parent.getItemAtPosition(position);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                SendParentMsgFragment.newInstance(itemMessages._name, itemMessages._msg_id, itemMessages._class_id
                        , itemMessages._stud_id)).addToBackStack("TAG_PARENT_SEND_MSG").commit();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class ItemMessages {
        String _msg_id;
        String _name;
        String _class;
        String _message;
        String _date;
        String _class_id;
        String _stud_id;
    }

    private class AsyncgetChats extends AsyncTask<String, Void, String> {

        ArrayList<ItemMessages> list;
        ItemMessages itemMessages;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url=Config.URL + Config.URL_PARENT_CMNCTN_API + "?StaffId=" + params[0] + "&AcademicYearId=" + params[1];
                JSONObject jsonObject = new JSONObject(dbControl.sendGetRequest(Config.URL + Config.URL_PARENT_CMNCTN_API
                        + "?StaffId=" + params[0] + "&AcademicYearId=" + params[1]));
                JSONArray jsonArray = jsonObject.getJSONArray("Notification");
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    itemMessages = new ItemMessages();
                    itemMessages._msg_id = jsonArray.getJSONObject(i).getString("Id");
                    itemMessages._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Name"));
                    itemMessages._class_id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("ClassId"));
                    itemMessages._stud_id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("StudentId"));
                    itemMessages._message = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Message"));
                    String _date = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("UpdateDate"));
                    Calendar calendar = staffControl.getDateByString(_date);
                    itemMessages._date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                    itemMessages._class = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Class"));
                    list.add(itemMessages);
                }
                return Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                return Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                if ((list != null) && (list.size() > 0)) {
                    setListAdapter(new AdapterChat(getContext(), 0, list));
                    dbHelper.updateNotif(Config.NOTIF_PARENT_CHAT, list.size(), 0);
                } else {
                    llNothingP.setVisibility(View.VISIBLE);
                }
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterChat extends ArrayAdapter<ItemMessages> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterChat(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemMessages> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_parent_chat, null);
                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvPCListName);
                viewHolder.tvClass = (TextView) convertView.findViewById(R.id.tvPCListClass);
                viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvPCListDate);
                viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvPCListMsg);
                convertView.setTag(viewHolder);
            }
            if ((position % 2) == 0) {
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#fcfcfc"));
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemMessages itemMessages = getItem(position);
            viewHolder.tvName.setText(itemMessages._name);
            viewHolder.tvMsg.setText(itemMessages._message);
            viewHolder.tvDate.setText(itemMessages._date);
            viewHolder.tvClass.setText(itemMessages._class);
            return convertView;
        }

        private class ViewHolder {
            TextView tvName, tvClass, tvDate, tvMsg;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncgetChats != null) && (asyncgetChats.getStatus().equals(AsyncTask.Status.RUNNING))) {
                asyncgetChats.cancel(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
