package com.eloitinnovations.edisappstaff;


/**
 * .3.59.229.107.6.178
 * aa t
 * aa k
 * Created by user on 28-03-2017.
 */

public class Config {

  //public static final String URL = BASE_URL + "/APIs/EdisappMob/";
   public static final String BASE_URL="https://mibv.edisapp.net";
   public static final String URL=Config.BASE_URL+"/APIs/EdisappMob/";


    //Common
    public static final String ERROR = "ERROR";
    public static final String SUCCESS = "SUCCESS";
    public static final String SHR_LOGIN = "SHR_Login";
    public static final String SHR_NOTIF = "SHR_Notif";

    public static final String INTENT_CLASS_ID = "ClassId";
    public static final String INTENT_STUD_ID = "StudId";

    //DbHelper Notif
    public static final String TABLE_NOTIF = "NotifTable";
    public static final String NOTIF_COUNT = "count";
    public static final String NOTIF_SHOW_NOTIF = "show_notif";
    public static final int NOTIF_ANNOUNC = 1;
    public static final int NOTIF_NEWS = 2;
    public static final int NOTIF_LEAVE = 3;
    public static final int NOTIF_SMS = 4;
    public static final int NOTIF_STAFF_CHATS = 5;
    public static final int NOTIF_PARENT_CHAT = 6;

    //URL's
    public static final String URL_STAFF_LOGIN = "StaffLogin/?";
    public static final String URL_ANNOUNCEMENT_LIST = "AnnouncementsList/?";
    public static final String URL_GET_ALL_NEWS_AND_EVENTS = "GetAllNewsAndEvents/?";
    public static final String URL_GET_NEWS_BY_ID = "GetNewsById/?";
    public static final String URL_GET_EVENTS_BY_ID = "GetEventsById/?";
    public static final String URL_GET_SMS_LIST = "GetSMSList/?";
    public static final String URL_GET_STAFF_LEAVE_TYPES = "getStaffLeaveTypes/?";
    public static final String URL_STAFF_LEAVES_API = "StaffLeavesAPI/?";
    public static final String URL_STAFF_LEAVE_API_APPLY = "StaffLeaveAPIApply";
    public static final String URL_GET_STAFF_CLASS_MAP = "GetStaffClassmapList/?";
    public static final String URL_STUDENT_ATTENDANCE_API = "StudentAttendanceAPI/?";
    public static final String URL_STUDENT_ATTENDANCE_MARK = "StudentAttendanceMarkAPI";
    public static final String URL_STUDENT_LEAVE = "StudentLeaveAPI/";
    public static final String URL_APROVE_STUDENT_LEAVE_API = "ApproveStudentLeaveAPI";
    public static final String URL_TIME_TABLE_API = "TimtableAPI/";
    public static final String URL_HOME_WORK_VIEW_API = "HomworkViewAPI/";
    public static final String URL_GET_SUBJECTS_LIST = "getClassSubjectList/";
    public static final String URL_CREATE_HW_API = "createHomeworkAPI";
    public static final String URL_ASSIGN_VIEW_API = "AssignmentViewAPI/";
    public static final String URL_CREATE_ASSIGN_API = "createAssignmentAPI";
    public static final String URL_GET_STAFF_LIST = "getStaffList/";
    public static final String URL_STAFF_COMNCTN_MSG_API = "StaffCommunicationMessageAPI/";
    public static final String URL_SND_STAFF_CMNCTN_MSG ="SendStaffCommunicationMessage";
    public static final String URL_GET_STDNT_DETAIL_BY_CLASS ="GetStudentdetailsByClass/";
    public static final String URL_MESSAGE_DETAILS = "MessageDetails/";
    public static final String URL_PARENT_CMNCTN_API = "ParentCommunicationMessageAPI/";
    public static final String URL_SEND_PARENT_MSG = "SendParentCommunicationMessageAPI";
    public static final String URL_SCHOOL_DETAILS = "Schooldetails/";

    public static final String INTENT_ID = "Id";
    public static final String INTENT_NAME = "Name";

    //SQLLite
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DbEloitEdisappStaff";

    //Notif
    public static final String DB_TABLE_NOTIFCECK = "DbNotifCheck";
    public static final String DB_NOTIFCHECK_STDID = "StdId";
    public static final String DB_NOTIFCHECK_ANNOUNC = "Announc";
    public static final String DB_NOTIFCHECK_NEWS = "News";
    public static final String DB_NOTIFCHECK_PARENT_CHAT = "ParentChat";
    public static final String DB_NOTIFCHECK_STAFF_CHAT="StaffChat";
    public static final String DB_NOTIFCHECK_SMS = "Sms";
    public static final String DB_NOTIFCHECK_LEAVE = "Leave";

    //JSON
    public static final String USERNAME = "UserName";
    public static final String PASSWORD = "Password";

    //JSON Login
    public static final String RESULT = "Result";
    public static final String ID = "Id";
    public static final String ACADEMIC_YEAR_ID = "AcademicYearId";
    public static final String NAME = "Name";
    public static final String LAST_LOGIN = "LastLogin";
    public static final String EMAIL = "Email";
    public static final String CURRENT_ADDRESS = "CurrentAddress";
    public static final String MOBILE = "mobile";
    public static final String GENDER = "Gender";
    public static final String APPOINMENT_DATE = "AppoinmentDate";
    public static final String CONFIRMATION_DATE = "ConfirmationDate";
    public static final String ADDRESS = "Address";
    public static final String EMERGENCY_CONTACT = "EmergencyContacts";
    public static final String MARITAL_STATUS = "MeritalStatus";
    public static final String DESIGNATION = "Designation";
    public static final String DATE_OF_BIRTH = "DateOfBirth";
    public static final String LOGIN_MESSAGE = "LoginMessage";
    public static final String INVALID_LOGIN = "Invalid credentials Invalid user";
    public static final String INVALID_CREDITIONLS = "Invalid Username/Password";
    public static final String NETWORK_ERROR = "Network Error Try Again";
    public static final String CLASS_NAME = "ClassName";

    //JSON Announcement
    public static final String STAFF_NAME = "StaffName";
    public static final String CONTENT = "Content";
    public static final String DATE = "Date";

    //JSON News And Events
    public static final String NEWS = "News";
    public static final String CREATED_DATE = "CreatedDate";
    public static final String TITLE = "Title";
    public static final String TYPE = "Type";

    //JSON SMS List
    public static final String STAFF_ID = "StaffId";

    //JSON LeaveApply Types
    public static final String LEAVE_TYPE = "LeaveType";

    //JSON LeaveApply
    public static final String FROM  = "From";
    public static final String TO = "To";
    public static final String STATUS = "Status";
    public static final String DAYS = "Days";
    public static final String REASON = "Reason";
    public static final String OFFICE_REMARK = "OfficeRemark";
    public static final String FROM_HALF = "Fromhalf";
    public static final String TO_HALF = "ToHalf";
    public static final String REQUESTED_DATE="RequestDate";
    public static final String LEAVE_ID = "LeaveId";
    public static final String FROM_CHECKED = "FromChecked";
    public static final String TO_CHECKED = "TOchecked";
    public static final String MESSAGE = "message";

    //JSON Attendance
    public static final String STANDARD = "Standard";
    public static final String PRESENT = "Present";
    public static final String ROLL_NO_C = "RollNO";
    public static final String CLASS_ID = "ClassId";
    public static final String STUDENT_ID =  "StudentId";
    public static final String POST_DATA = "postData";
    public static final String MARK_ATT = "markatt";

    //JSON Time Table
    public static final String SLOTS = "Slots";
    public static final String DAY = "Day";
    public static final String PEROID_NAME = "PeriodName";
    public static final String START_TIME = "startTime";
    public static final String END_TIME = "EndTime";
    public static final String SUBJECT = "Subject";

    //JSON School Details
    public static final String SCHOOL_NAME = "SchoolName";
    public static final String SCHOOL_ADDRESS = "SchoolAddress";
    public static final String SCHOOL_CONTACT = "SchoolContact";
    public static final String SCHOOL_WEBSITE = "SchoolWebsite";
    public static final String SCHOOL_EMAIL = "SchoolEmail";


}
