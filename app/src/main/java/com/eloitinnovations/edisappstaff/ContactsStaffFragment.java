package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ContactsStaffFragment extends ListFragment implements AdapterView.OnItemClickListener {


    private ArrayList<ItemContacts> list;
    ArrayList<ItemContacts> tempList = new ArrayList<>();
    private EditText ET_search;
    private TextView tvNothingP;

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private ProgressBar pbTitle;
    private AsyncGetContacts asyncGetContacts;
    private OnFragmentInteractionListener mListener;

    public ContactsStaffFragment() {
        // Required empty public constructor
    }

    public static ContactsStaffFragment newInstance(String param1, String param2) {

        ContactsStaffFragment fragment = new ContactsStaffFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts_staff, container, false);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        ET_search = view.findViewById(R.id.et_contacts_staff_search);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pbTitle.setVisibility(View.GONE);
        if ((asyncGetContacts != null) && (asyncGetContacts.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncGetContacts.cancel(true);
        asyncGetContacts = new AsyncGetContacts();
        asyncGetContacts.execute();
        getListView().setOnItemClickListener(this);
        ET_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    if(list!=null) {
                        setListAdapter(new AdapterContacts(getContext(), 0, list));
                        getListView().deferNotifyDataSetChanged();
                    }
                } else {
                    searchOnList(s.toString());
                }
            }
        });
    }

    private void searchOnList(String s) {
        if (s.isEmpty())
            return;

        if (list == null || list.size() == 0)
            return;

        tempList.clear();
        for (ItemContacts temp : list){
            if(temp._name.toLowerCase().contains(s.toLowerCase()))
                tempList.add(temp);
        }

        setListAdapter(new AdapterContacts(getContext(),0,tempList));
        getListView().deferNotifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncGetContacts != null) && (asyncGetContacts.getStatus().equals(AsyncTask.Status.RUNNING))) {
                asyncGetContacts.cancel(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ItemContacts itemContacts = (ItemContacts) parent.getItemAtPosition(position);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                StaffSendMessageFragment.newInstance(itemContacts._id, itemContacts._name, "")).addToBackStack("TAG_STAFF_SEND_MSG").commit();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class ItemContacts {
        String _id;
        String _name;
        String _designation;
    }

    private class AsyncGetContacts extends AsyncTask<Void, Void, String> {

        String _requestURL;
        ItemContacts itemContacts;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _requestURL = Config.URL + Config.URL_GET_STAFF_LIST;
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    itemContacts = new ItemContacts();
                    itemContacts._id = staffControl.getNullStr(jsonObject.getString("Id"));
                    itemContacts._name = staffControl.getNullStr(jsonObject.getString("Name"));
                    itemContacts._designation = staffControl.getNullStr(jsonObject.getString("Designation"));
                    list.add(itemContacts);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                setListAdapter(new AdapterContacts(getContext(), 0, list));

            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterContacts extends ArrayAdapter<ItemContacts> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterContacts(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemContacts> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_contacts_staff, null);
                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvSCListName);
                viewHolder.tvDesg = (TextView) convertView.findViewById(R.id.tvSCListDesg);
                convertView.setTag(viewHolder);
            }
            if ((position % 2) == 0) {
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#fcfcfc"));
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemContacts itemContacts = getItem(position);
            viewHolder.tvName.setText(itemContacts._name);
            viewHolder.tvDesg.setText(itemContacts._designation);
            return convertView;
        }


        private class ViewHolder {
            TextView tvName;
            TextView tvDesg;
        }
    }

}
