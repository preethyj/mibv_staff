package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class TimeTableManinFragment extends Fragment implements TabLayout.OnTabSelectedListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPager viewPagerClassStaff;
    private PagerTimeTable pagerTimeTable;
    private ProgressBar pbTitle;
    private Button btnToClass, btnToStaff;
    private ImageButton btnToStaffImg, btnToClassImg;
    private Spinner spClasses;
    private Spinner spStandard;

    private ArrayList<SpClassItems> listClass;
    private SharedPreferences spLogin;
    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private AsyncClassList asyncClassList;
    private AsyncGetTimeTable asyncGetTimeTable;

    private String _staff_id;
    private String _class_id;
    private Boolean isStaff;
    private OnFragmentInteractionListener mListener;

    private LinearLayout llTimeTableClassMenu;
    private LinearLayout llTimeTableChoose;

    public TimeTableManinFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TimeTableManinFragment newInstance() {
        TimeTableManinFragment fragment = new TimeTableManinFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_table_main, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutTimeTable);
        viewPager = (ViewPager) view.findViewById(R.id.pagerTimeTable);
        viewPagerClassStaff = (ViewPager) view.findViewById(R.id.viewpagerTTClassStaff);
        btnToClass = (Button) view.findViewById(R.id.btnTTToClass);
        btnToStaff = (Button) view.findViewById(R.id.btnTTToStaff);
        btnToStaffImg = (ImageButton) view.findViewById(R.id.btnTTToStaffImg);
        btnToClassImg = (ImageButton) view.findViewById(R.id.btnTTToClassImg);
        spClasses = (Spinner) view.findViewById(R.id.spTTClass);
        spStandard = (Spinner) view.findViewById(R.id.spTTStandard);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llTimeTableClassMenu = (LinearLayout) view.findViewById(R.id.llTimeTableClassMenu);
        llTimeTableChoose = (LinearLayout) view.findViewById(R.id.llTimeTableChoose);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Timetable");
        btnHome.setVisibility(View.VISIBLE);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN,Context.MODE_PRIVATE);
        spClasses.setOnItemSelectedListener(this);
        spStandard.setOnItemSelectedListener(this);
        btnToClassImg.setOnClickListener(this);
        btnToStaff.setOnClickListener(this);
        btnToStaffImg.setOnClickListener(this);
        btnToClass.setOnClickListener(this);
        viewPagerClassStaff.setAdapter(new MenuPagerAdapter());
        viewPagerClassStaff.setCurrentItem(0);

        _staff_id = spLogin.getString(Config.ID, null);

        isStaff = true;
        if (asyncClassList != null) {
            if (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)) {
                asyncClassList.cancel(true);
            }
        }
        if (asyncGetTimeTable != null) {
            if (asyncGetTimeTable.getStatus().equals(AsyncTask.Status.RUNNING)) {
                asyncGetTimeTable.cancel(true);
            }
        }
        asyncGetTimeTable = new AsyncGetTimeTable();
        asyncGetTimeTable.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Time_Tble_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        //       try {
        viewPager.setCurrentItem(tab.getPosition());
        View view = tab.getCustomView();
        if (view != null) {
            TextView tvDay = (TextView) view.findViewById(R.id.tvTimeTableTabHCur);
            tvDay.setTextSize(18);
            tvDay.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            tvDay.setAlpha(1);
        }
        //     }catch (Exception e){
        //         e.printStackTrace();
        //    }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        //   try {
        View view = tab.getCustomView();
        if (view != null) {
            TextView tvDay = (TextView) view.findViewById(R.id.tvTimeTableTabHCur);
            tvDay.setTextSize(14);
            tvDay.setTextColor(Color.parseColor("#1e1d1d"));
            tvDay.setAlpha((float) 0.45);
        }
        //    }catch (Exception e){
        //        e.printStackTrace();
        //    }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        if ((v == btnToClass) || (v == btnToClassImg)) {
            viewPagerClassStaff.setCurrentItem(1);
            isStaff = false;
            if (asyncClassList != null) {
                if (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)) {
                    asyncClassList.cancel(true);
                }
            }
            asyncClassList = new AsyncClassList();
            asyncClassList.execute();
        } else if ((v == btnToStaff) || (v == btnToStaffImg)) {
            viewPagerClassStaff.setCurrentItem(0);
            isStaff = true;
            if (asyncClassList != null) {
                if (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)) {
                    asyncClassList.cancel(true);
                }
            }
            if (asyncGetTimeTable != null) {
                if (asyncGetTimeTable.getStatus().equals(AsyncTask.Status.RUNNING)) {
                    asyncGetTimeTable.cancel(true);
                }
            }
            asyncGetTimeTable = new AsyncGetTimeTable();
            asyncGetTimeTable.execute();

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == spStandard.getId()) {
            String _standard = (String) parent.getItemAtPosition(position);
            initSpClass(listClass, _standard);
        } else if (parent.getId() == spClasses.getId()) {
            SpClassItems spClassItems = (SpClassItems) parent.getItemAtPosition(position);
            _class_id = spClassItems._id;
            // new AsyncGetAttendance().execute(spClassItems._id);
            isStaff = false;
            if (asyncGetTimeTable != null) {
                if (asyncGetTimeTable.getStatus().equals(AsyncTask.Status.RUNNING)) {
                    asyncGetTimeTable.cancel(true);
                }
            }
            asyncGetTimeTable = new AsyncGetTimeTable();
            asyncGetTimeTable.execute();

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class MenuPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }


        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1; // return true if both are equal.
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            switch (position) {
                case 1:
                    return llTimeTableClassMenu;
                default:
                case 0:
                    return llTimeTableChoose;
            }

        }

    }

    private class SpClassItems {
        String _name;
        String _id;
        String _standard;
    }

    private class AsyncClassList extends AsyncTask<Void, Void, String> {

        String _requestURL;
        SpClassItems spClassItems;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            //lvStudents.setAdapter(null);
            _requestURL = Config.URL + Config.URL_GET_STAFF_CLASS_MAP + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null) +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
          //  System.out.println("-->Request URL "+_requestURL);
            listClass = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    spClassItems = new SpClassItems();
                    spClassItems._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                    spClassItems._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.NAME));
                    spClassItems._standard = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STANDARD));
                    listClass.add(spClassItems);
                }
                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    initSpStandard(listClass);
                    break;
                case Config.ERROR:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private void initSpClass(ArrayList<SpClassItems> list, String _Standard) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<SpClassItems> listByStand = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (spClassItems._standard.equals(_Standard)) {
                    listByStand.add(spClassItems);
                }
            }
            if ((listByStand != null) && (listByStand.size() > 0))
                spClasses.setAdapter(null);
            spClasses.setAdapter(new AdapterSPClass(getContext(), R.layout.sp_item, listByStand));
        }
    }

    private void initSpStandard(ArrayList<SpClassItems> list) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listStandard = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (!listStandard.contains(spClassItems._standard)) {
                    listStandard.add(spClassItems._standard);
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.sp_item, listStandard);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spStandard.setAdapter(adapter);
        }
    }

    private class AdapterSPClass extends ArrayAdapter<SpClassItems> {

        LayoutInflater inflater;
        ViewHolder holder;

        public AdapterSPClass(@NonNull Context context, @LayoutRes int resource, @NonNull List<SpClassItems> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        private class ViewHolder {
            TextView tvItem;
        }
    }

    private class AsyncGetTimeTable extends AsyncTask<Void, Void, String> {

        ArrayList<ItemDays> listDays;
        String _requestURL;
        String _Astaff_id;
        String _Aclass_id;
        ItemDays itemDays;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            if (isStaff) {
                _Aclass_id = "0";
                _Astaff_id = _staff_id;
            } else {
                _Aclass_id = _class_id;
                _Astaff_id = "0";
            }
            _requestURL = Config.URL + Config.URL_TIME_TABLE_API + "?classId=" + _Aclass_id + "&staffId=" + _Astaff_id;
         //   System.out.println("request URL "+_requestURL);
            listDays = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArrayDays = new JSONArray(_response);
                for (int i = 0; i < jsonArrayDays.length(); i++) {

                    JSONObject jsonObjectDays = jsonArrayDays.getJSONObject(i);
                    itemDays = new ItemDays();
                    itemDays._day = jsonObjectDays.getString(Config.DAY);
                    JSONArray arraySlots = jsonObjectDays.getJSONArray(Config.SLOTS);
                    itemDays.itemTimeTables = new ArrayList<>();
                    for (int j = 0; j < arraySlots.length(); j++) {
                        ItemTimeTable itemTimeTable = new ItemTimeTable();
                        itemTimeTable._period_name = staffControl.getNullStr(arraySlots.getJSONObject(j).getString(Config.PEROID_NAME));
                        itemTimeTable._start_time = staffControl.getNullStr(arraySlots.getJSONObject(j).getString(Config.START_TIME));
                        itemTimeTable._end_time = staffControl.getNullStr(arraySlots.getJSONObject(j).getString(Config.END_TIME));
                        itemTimeTable._staff_name = staffControl.getNullStr(arraySlots.getJSONObject(j).getString(Config.STAFF_NAME));
                        itemTimeTable._subject = staffControl.getNullStr(arraySlots.getJSONObject(j).getString(Config.SUBJECT));
                        itemTimeTable._class_name = staffControl.getNullStr(arraySlots.getJSONObject(j).getString("Classname"));
                        itemDays.itemTimeTables.add(itemTimeTable);
                    }
                    listDays.add(itemDays);
                }

                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            initViewAdapter(listDays);
            pbTitle.setVisibility(View.GONE);
        }
    }

    private void initViewAdapter(ArrayList<ItemDays> listDays) {
        try{
        if(listDays.size()==5) {
            ItemDays extraDay=new ItemDays();
            extraDay.position=6;
            extraDay._day="Saturday";
            extraDay.itemTimeTables=new ArrayList<>();
            listDays.add(5,extraDay);
            pagerTimeTable = new PagerTimeTable(getActivity().getSupportFragmentManager(), getContext(), 6, listDays);
        }
        else if(listDays.size()==6)
        {
            pagerTimeTable = new PagerTimeTable(getActivity().getSupportFragmentManager(), getContext(), 6, listDays);
        }
        else {
            //nothing
        }}catch (Exception e){
            e.printStackTrace();
        }
        viewPager.setAdapter(pagerTimeTable);
       // tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerTimeTable.getTabView(i));
        }
        viewPager.setCurrentItem(((Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1)<0?0:(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING))) {
                asyncClassList.cancel(true);
            }
            if ((asyncGetTimeTable != null) && (asyncGetTimeTable.getStatus().equals(AsyncTask.Status.RUNNING))) {
                asyncGetTimeTable.cancel(true);
            }
        } catch (Exception e) {
           // e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
