package com.eloitinnovations.edisappstaff;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;


public class SchoolDetailsFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private DbControl dbControl = new DbControl();

    private String _schoolName;
    private String _contact;
    private String _website;
    private String _email;
    private String _address;

    private TextView tvSchoolName;
    private TextView tvWebSite;
    private TextView tvAddress;
    private TextView tvContact;
    private TextView tvEmail;

    private ProgressBar progressBar;
    private AsyncSchool asyncSchool;
    private ImageButton ibCall;
    private ImageView ivSchool;

    private boolean isPermissionToCall = false;
    public final static int PERMISSION_TO_CALL = 121;

    public SchoolDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static SchoolDetailsFragment newInstance() {
        SchoolDetailsFragment fragment = new SchoolDetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_details, container, false);
        tvAddress = (TextView) view.findViewById(R.id.tvSchoolAddress);
        tvContact = (TextView) view.findViewById(R.id.tvSchoolContact);
        tvEmail = (TextView) view.findViewById(R.id.tvSchoolEmail);
        tvSchoolName = (TextView) view.findViewById(R.id.tvSchoolDetailsSchlName);
        tvWebSite = (TextView) view.findViewById(R.id.tvSchoolWebsite);
        progressBar = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        ibCall = (ImageButton) view.findViewById(R.id.ibCall);
        ibCall.setVisibility(View.GONE);
        ivSchool = (ImageView) view.findViewById(R.id.ivSchool);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("School Profile");
        btnHome.setVisibility(View.VISIBLE);
        if((asyncSchool!=null)&&(asyncSchool.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncSchool.cancel(true);
        asyncSchool=new AsyncSchool();
        asyncSchool.execute();
        try {
            SharedPreferences spLogin =getActivity().getSharedPreferences(Config.SHR_LOGIN,Context.MODE_PRIVATE);
            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Schl_Det_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //method to check permission
        checkPermission();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_TO_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionToCall = true;

                } else {
                    isPermissionToCall = false;
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void checkPermission() {
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PERMISSION_TO_CALL);
    }
    private class AsyncSchool extends AsyncTask<Void, Void, String>{

        String requestURL;
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            _address = "";
            _contact="";
            _email = "";
            _schoolName="";
            _website="";
            requestURL = Config.URL + Config.URL_SCHOOL_DETAILS;
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = "ERROR";
            try {
                _response = dbControl.sendGetRequest(requestURL);
                JSONObject jsonObject = new JSONObject(_response);
                _address = jsonObject.getString(Config.SCHOOL_ADDRESS);
                _schoolName = jsonObject.getString(Config.SCHOOL_NAME);
                _contact = jsonObject.getString(Config.SCHOOL_CONTACT);
                _email = jsonObject.getString(Config.SCHOOL_EMAIL);
                _website = jsonObject.getString(Config.SCHOOL_WEBSITE);
            } catch (JSONException e) {
                e.printStackTrace();
                _response = "ERROR";
            }catch (Exception e){
                e.printStackTrace();
                _response = "ERROR";
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("ERROR") || s.isEmpty()||s.equals("null")){

            }else{
                initTextView();
            }
            progressBar.setVisibility(View.GONE);
        }
    }
    private void initTextView() {
        String imgURL = Config.BASE_URL + "/Content/images/school_logo.png";
        Picasso.get().
                load(imgURL).
                into(ivSchool);
        tvAddress.setText(setNull(_address));
        tvContact.setText(setNull(_contact));
        final String contact = setNull(_contact);
        if (tvContact.getText().equals("")) {
            ibCall.setVisibility(View.GONE);
        } else {
            ibCall.setVisibility(View.VISIBLE);
            ibCall.setClickable(true);
            ibCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (isPermissionToCall) {
                            String phoneNumber = String.format("tel: %s",
                                    tvContact.getText().toString());
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse(phoneNumber));
                            startActivity(callIntent);
                        } else {
                            checkPermission();
                        }
                    } catch (ActivityNotFoundException e) {
                        System.out.println("Exception" + e);
                    }
                }
            });


        }
        tvEmail.setText(setNull(_email));
        tvSchoolName.setText(setNull(_schoolName));
        tvWebSite.setText(setNull(_website));
    }

    private String setNull(String _item){
        if(_item.equals("null")){
            return "";
        }else {
            return _item;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncSchool != null) && (asyncSchool.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncSchool.cancel(true);
        }catch (Exception e){

        }
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
