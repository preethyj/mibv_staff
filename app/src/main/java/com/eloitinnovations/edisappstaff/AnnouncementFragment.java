package com.eloitinnovations.edisappstaff;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class AnnouncementFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private DbControl dbControl;
    private StaffControl staffControl;
    private DbHelper dbHelper;

    private Dialog dialog;
    private ProgressBar pbTBAsync;
    private LinearLayout llNothingP;
    private ListView lvAnnounce;
    private TextView tvNothingHasP;
    private SharedPreferences spLogin;
    private AsyncAnnouncements asyncAnnouncements;


    public AnnouncementFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static AnnouncementFragment newInstance() {
        AnnouncementFragment fragment = new AnnouncementFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_announcement, container, false);
        lvAnnounce = (ListView) view.findViewById(R.id.lvAncmntList);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        pbTBAsync = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        tvNothingHasP = (TextView) view.findViewById(R.id.tvNothingHasp);
        tvNothingHasP.setText("No announcements yet.");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Announcements");
        btnHome.setVisibility(View.VISIBLE);

        dbControl = new DbControl();
        staffControl = new StaffControl();
        dbHelper = new DbHelper(getContext());
        dialog = new Dialog(getContext(), android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle("Announcement");

        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        pbTBAsync.setVisibility(View.GONE);
        if ((asyncAnnouncements != null) && (asyncAnnouncements.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncAnnouncements.cancel(true);
        asyncAnnouncements = new AsyncAnnouncements();
        asyncAnnouncements.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Announc_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class EachRow {
        String _id;
        String _content;
        String _month;
        String _day;
        String _date;
        String _staff;
        ArrayList<String> urlList = new ArrayList<String>();
    }

    private class AsyncAnnouncements extends AsyncTask<Void, Void, String> {

        private ArrayList<EachRow> list;
        String _requestURL;

        @Override
        protected void onPreExecute() {
            pbTBAsync.setVisibility(View.VISIBLE);
            _requestURL = Config.URL + Config.URL_ANNOUNCEMENT_LIST + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                if (!_response.equals("CONNECTION_ERROR")) {
                    JSONArray jsonArray = new JSONArray(_response);
                    EachRow eachRow;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        eachRow = new EachRow();
                        eachRow._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                        eachRow._content = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.CONTENT));
                        String _date = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.DATE));
                        eachRow._staff = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STAFF_NAME));
                        eachRow._month = _date.substring(0, 3);
                        eachRow._day = _date.substring(4);
                        JSONArray urlsArray = jsonArray.getJSONObject(i).getJSONArray("URLList");
                        if (urlsArray != null)
                            for (int j = 0; j < urlsArray.length(); j++)
                                eachRow.urlList.add(urlsArray.getString(j));
                        list.add(eachRow);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                _response = "ERROR";
            } catch (Exception e) {
                e.printStackTrace();
                _response = "ERROR";
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            pbTBAsync.setVisibility(View.GONE);
            if (!(s.equals("CONNECTION_ERROR") || s.equals("ERROR") || s.equals("NULL") || s.isEmpty())) {
                initListItems(list);
            }
        }
    }

    private class AdapterAnnounce extends ArrayAdapter<EachRow> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterAnnounce(@NonNull Context context, @LayoutRes int resource, @NonNull List<EachRow> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }


        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_announcement, null);
                viewHolder = new ViewHolder();
                viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tvAncmntListContent);
                viewHolder.tvMonthName = (TextView) convertView.findViewById(R.id.tvAnnouncementMonthName);
                viewHolder.tvMonthNum = (TextView) convertView.findViewById(R.id.tvAnnouncementMonthNum);
                viewHolder.tvStaffName = (TextView) convertView.findViewById(R.id.tvAncmntListStaff);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            EachRow row = (EachRow) getItem(position);
            viewHolder.tvContent.setText(row._content);
            viewHolder.tvStaffName.setText(row._staff);
            viewHolder.tvMonthName.setText(row._month);
            viewHolder.tvMonthNum.setText(row._day);
            return convertView;
        }

        private class ViewHolder {
            TextView tvMonthName;
            TextView tvMonthNum;
            TextView tvContent;
            TextView tvStaffName;
        }
    }

    private void initListItems(final ArrayList<EachRow> list) {
        if ((list != null) && (list.size() > 0)) {
            llNothingP.setVisibility(View.GONE);
            lvAnnounce.setAdapter(new AdapterAnnounce(getContext(), 0, list));
            dbHelper.updateNotif(Config.NOTIF_ANNOUNC, list.size(), 0);
            lvAnnounce.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    EachRow row = (EachRow) parent.getItemAtPosition(position);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                            AnnouncementDetailFragment.newInstance(row._staff, row._content, row._day, row._month, row.urlList)).commit();
                }
            });
        } else {
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncAnnouncements != null) && (asyncAnnouncements.getStatus() == AsyncTask.Status.RUNNING)) {
                asyncAnnouncements.cancel(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}




