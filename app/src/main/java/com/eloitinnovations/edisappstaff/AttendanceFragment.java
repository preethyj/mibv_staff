package com.eloitinnovations.edisappstaff;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class AttendanceFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();

    private ProgressBar pbTitle;
    private SharedPreferences spLogin;
    private TextView tvAbsnts,tvStrngth,tvChngFont,tvCurrentDate;
    private ListView lvViewAttend;

    private AsyncGetAttendance asyncGetAttendance;

    private String _strId = "";
    private String _class_id = "";


    public AttendanceFragment() {
        // Required empty public constructor
    }


    public static AttendanceFragment newInstance() {
        AttendanceFragment fragment = new AttendanceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        tvAbsnts = (TextView) view.findViewById(R.id.tvAttenListAbsnt);
        tvStrngth = (TextView) view.findViewById(R.id.tvAttendListStrngth);
        lvViewAttend = (ListView) view.findViewById(R.id.lvAttendViewList);
        tvChngFont = (TextView) view.findViewById(R.id.tvChngFont);
        tvCurrentDate = (TextView) view.findViewById(R.id.tvAttendViewDate);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        try{
            Typeface customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/BEBAS___.TTF");
            tvAbsnts.setTypeface(customFont);
            tvStrngth.setTypeface(customFont);
            tvChngFont.setTypeface(customFont);
            tvCurrentDate.setTypeface(customFont);
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            Calendar calendar = Calendar.getInstance();
           SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            tvCurrentDate.setText(df.format(calendar.getTime()));
        }catch (Exception e){
            e.printStackTrace();
        }

        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Attendance "+spLogin.getString(Config.CLASS_NAME, null));
        btnHome.setVisibility(View.VISIBLE);
        _class_id = spLogin.getString(Config.CLASS_ID, null);
        if ((_class_id != null) && (!_class_id.equals("0")) && (!_class_id.isEmpty())) {
            if((asyncGetAttendance!=null)&&(asyncGetAttendance.getStatus() == AsyncTask.Status.RUNNING))
                asyncGetAttendance.cancel(true);
            asyncGetAttendance = new AsyncGetAttendance();
            asyncGetAttendance.execute(_class_id);
        }
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Attndnc_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class Students {
        String _student_id;
        String _roll_no;
        String _name;
        boolean isPresent = true;
    }

    private class AsyncGetAttendance extends AsyncTask<String, Void, String> {

        String _requestURl;
        String _class_id;
        String _academic_year_id;
        ArrayList<Students> list;
        Students students;
        int prsnt,strnth;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
            _requestURl = Config.URL + Config.URL_STUDENT_ATTENDANCE_API;
            prsnt = 0;
            strnth = 0;
            _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
        }

        @Override
        protected String doInBackground(String... params) {
            _class_id = params[0];
            _requestURl += Config.CLASS_ID + "=" + _class_id + "&" + Config.ACADEMIC_YEAR_ID + "=" + _academic_year_id;
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURl);
                JSONObject jsonObject = new JSONObject(_response);
                JSONArray jsonArray = jsonObject.getJSONArray("datavalue");
                strnth = jsonArray.length();
                prsnt = jsonArray.length();
                for (int i = 0; i < jsonArray.length(); i++) {
                    students = new Students();
                    students._student_id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STUDENT_ID));
                    students._roll_no = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ROLL_NO_C));
                    students._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.NAME));
                    students.isPresent = jsonArray.getJSONObject(i).getBoolean(Config.PRESENT);
                    if (!students.isPresent) {
                        addStr(students._student_id);
                        prsnt--;
                        students.isPresent = false;
                        list.add(students);
                    }
                }
                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    try {
                        tvStrngth.setText(""+ strnth);
                        tvAbsnts.setText(""+prsnt);
                        initListStudents(list);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case Config.ERROR:
                default:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterLVStudents extends ArrayAdapter<Students> implements View.OnClickListener {

        LayoutInflater inflater;
        ViewHolder viewHolder;
        public AdapterLVStudents(@NonNull Context context, @LayoutRes int resource, @NonNull List<Students> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_attendance_view, null);
                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvlistAttnViewName);
                viewHolder.tvRollNo = (TextView) convertView.findViewById(R.id.tvlistAttenViewRollNo);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            Students student = getItem(position);
            viewHolder.tvName.setText(student._name);
            viewHolder.tvRollNo.setText(student._roll_no);
            viewHolder.position = position;
            return convertView;
        }

        @Override
        public void onClick(View v) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            Students student = getItem(viewHolder.position);

        }

        private class ViewHolder {
            TextView tvRollNo;
            TextView tvName;
            int position;
        }
    }

    private class AsyncSendAttendance extends AsyncTask<String, Void, String> {

        String _staff_id;
        String _academic_year_id;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _staff_id = spLogin.getString(Config.ID, null);
            _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return dbControl.sendAttendance(params[0], _class_id, _academic_year_id, _staff_id);

            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    Toast.makeText(getContext(), "Attendance Marked", Toast.LENGTH_SHORT).show();
                    break;
                case Config.ERROR:
                default:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private void initListStudents(ArrayList<Students> list) {
        if ((list != null) && (list.size() > 0)) {
            lvViewAttend.setAdapter(null);
            lvViewAttend.setAdapter(new AdapterLVStudents(getContext(), 0, list));
        }
    }

    private void addStr(String s) {
        if (!_strId.contains(s + ",F*")) {
            _strId += s + ",F*";
        }
    }

    private void delStr(String s) {
        if (_strId.contains(s + ",F*")) {
            _strId = _strId.replace(s + ",F*", "");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if((asyncGetAttendance!=null)&&(asyncGetAttendance.getStatus() == AsyncTask.Status.RUNNING))
                asyncGetAttendance.cancel(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
