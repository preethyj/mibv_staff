package com.eloitinnovations.edisappstaff;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener, SchoolDetailsFragment.OnFragmentInteractionListener,
        PrincipalDeskFragment.OnFragmentInteractionListener, AnnouncementFragment.OnFragmentInteractionListener,
        NewsFragment.OnFragmentInteractionListener, AttendanceFragment.OnFragmentInteractionListener,
        TabStaffLeave.OnFragmentInteractionListener, TabStudentLeave.OnFragmentInteractionListener,
        LeaveFragment.OnFragmentInteractionListener, ChatsStaffMainFragment.OnFragmentInteractionListener,
        ChatsStaffFragments.OnFragmentInteractionListener, ChatsStaffOutBoxFragment.OnFragmentInteractionListener,
        ContactsStaffFragment.OnFragmentInteractionListener, TimeTableManinFragment.OnFragmentInteractionListener,
        TimeTableFragment.OnFragmentInteractionListener, SMSFragment.OnFragmentInteractionListener,
        ChatsParentFragment.OnFragmentInteractionListener, ContactsParentsFragment.OnFragmentInteractionListener,
        ChatsParentMainFragment.OnFragmentInteractionListener, AssignmentFragment.OnFragmentInteractionListener,
        HomeworkFragment.OnFragmentInteractionListener,ViewFilesFragment.OnFragmentInteractionListener,AnnouncementDetailFragment.OnFragmentInteractionListener,
        AboutUsFragment.OnFragmentInteractionListener,StaffSendMessageFragment.OnFragmentInteractionListener,
        SendParentMsgFragment.OnFragmentInteractionListener,TabEventsFragment.OnFragmentInteractionListener,
        TabNewsFragment.OnFragmentInteractionListener{

    private SharedPreferences spLogin;
    private TextView tvNavName;
    private TextView tvNavEmail;
    private ImageButton btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnActionBarMainHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        tvNavName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvNavMainName);
        tvNavEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvNavMainEmail);
        tvNavEmail.setText(spLogin.getString(Config.EMAIL, null));
        tvNavName.setText(spLogin.getString(Config.NAME, null));

        Fragment fragment = null;
        Class fragmentClass = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentClass = HomeFragment.class;
            fragment = (Fragment) fragmentClass.newInstance();
            fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment, "Home").addToBackStack(null).commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (spLogin.getAll().size() < 16) {
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        } catch (Exception e) {
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            finish();
        }
        FirebaseMessaging.getInstance().subscribeToTopic("mibvedisappnet");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {

                if ((getSupportFragmentManager().getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 1).getName() != null)) {
                    switch ((getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName())) {
                        case "TAG_STAFF_SEND_MSG":
                        case "STAFF_VIEW_MSG":
                            getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity, ChatsStaffMainFragment.newInstance()).addToBackStack(null).commit();
                            break;
                        case "TAG_PARENT_SEND_MSG":
                            getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity, ChatsParentMainFragment.newInstance()).addToBackStack(null).commit();
                            break;
                        case "TAG_ANNOUNCEMENT":
                            getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity, AnnouncementFragment.newInstance()).addToBackStack(null).commit();
                        default:
                            break;
                    }
                } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                            HomeFragment.newInstance()).addToBackStack(null).commit();
                }
            } else
                finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            if (id == R.id.nav_home) {
                fragmentClass = HomeFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment).addToBackStack("NAME_HOME").commit();
            } else if (id == R.id.nav_profile) {
                fragmentClass = ProfileFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment, null).addToBackStack(null).commit();
            } else if (id == R.id.nav_school_details) {
                fragmentClass = SchoolDetailsFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment, null).addToBackStack(null).commit();

            } /*else if (id == R.id.nav_Principal) {
                fragmentClass = PrincipalDeskFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment, null).addToBackStack(null).commit();
            }*/  else if (id == R.id.nav_About_us) {
                fragmentClass = AboutUsFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
                fragmentManager.beginTransaction().replace(R.id.flHomeActivity, fragment, null).addToBackStack(null).commit();
            } else if (id == R.id.nav_logout) {
                SharedPreferences spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
                spLogin.edit().clear().apply();
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(HomeActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(HomeActivity.this, NotificationService.class));
    }
}
