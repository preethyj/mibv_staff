package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class AssignmentFragment extends Fragment {



    private SharedPreferences spLogin;
    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();


    private ProgressBar pbTitle;
    private ListView lvAssign;
    private Button btnCreate;
    private LinearLayout llNothingP;

    private String _staff_id;
    private String _academic_year_id;

    private AsynGetAssign asynGetAssign;

    private OnFragmentInteractionListener mListener;

    public AssignmentFragment() {
        // Required empty public constructor
    }

    public static AssignmentFragment newInstance() {
        AssignmentFragment fragment = new AssignmentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_assignment, container, false);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        lvAssign = (ListView) view.findViewById(R.id.lvAssigmnentsList);
        btnCreate = (Button) view.findViewById(R.id.btnAssignCreate);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Assignments");
        btnHome.setVisibility(View.VISIBLE);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),AssignmentCreateActivity.class));
            }
        });
        asynGetAssign = new AsynGetAssign();
        asynGetAssign.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Assign_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private class ItemAssign {
        String _day;
        String _month;
        String _year;
        String _class;
        String _subject;
        String _title;
        String _desc;
    }

    private class AsynGetAssign extends AsyncTask<Void, Void, String> {

        String _requestURL;
        ArrayList<ItemAssign> list;
        ItemAssign itemAssign;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _requestURL = Config.URL + Config.URL_ASSIGN_VIEW_API + "?" + Config.STAFF_ID + "=" + _staff_id
                    + "&" + Config.ACADEMIC_YEAR_ID + "=" + _academic_year_id;
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    itemAssign = new ItemAssign();
                    itemAssign._class = staffControl.getNullStr(jsonObject.getString("AssignedToName"));
                    itemAssign._title = staffControl.getNullStr(jsonObject.getString("Title"));
                    itemAssign._desc = staffControl.getNullStr(jsonObject.getString("Description"));
                    itemAssign._subject = staffControl.getNullStr(jsonObject.getString("Subject"));
                    String _date = staffControl.getNullStr(jsonObject.getString("LastDate"));
                    itemAssign._day = _date.substring(0, _date.indexOf("/"));
                    itemAssign._month = staffControl.getMonthById(Integer.valueOf(_date.substring(_date.indexOf("/") + 1, _date.lastIndexOf("/"))) - 1);
                    itemAssign._year = _date.substring(_date.lastIndexOf("/") + 1, _date.length());
                    list.add(itemAssign);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initList(list);
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterAssign extends ArrayAdapter<ItemAssign> implements View.OnClickListener {
        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterAssign(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemAssign> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_assignments, null);
                viewHolder = new ViewHolder();
                viewHolder.tvClass = (TextView) convertView.findViewById(R.id.tvAssignListClass);
                viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tvAssignListDay);
                viewHolder.tvDesc = (TextView) convertView.findViewById(R.id.tvAssignListDesc);
                viewHolder.tvMonth = (TextView) convertView.findViewById(R.id.tvAssignListMonth);
                viewHolder.tvSub = (TextView) convertView.findViewById(R.id.tvAssignListSubject);
                viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvAssignListTitle);
                viewHolder.tvYear = (TextView) convertView.findViewById(R.id.tvAssignListYear);
                viewHolder.btnViewMore = (Button) convertView.findViewById(R.id.btnAssignListViewMore);
                viewHolder.llViewMore = (LinearLayout) convertView.findViewById(R.id.llAssignViewMore);
                viewHolder.btnViewMore.setTag(viewHolder);
                viewHolder.btnViewMore.setOnClickListener(this);
                viewHolder.position = position;
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemAssign item = getItem(position);
            viewHolder.tvClass.setText(item._class);
            viewHolder.tvDay.setText(item._day);
            viewHolder.tvDesc.setText(item._desc);
            viewHolder.tvMonth.setText(item._month);
            viewHolder.tvSub.setText(item._subject);
            String _title = "";
            if ((!item._title.isEmpty()) && (item._title.length() > 50)) {
                _title = item._title.substring(0, 45) + "...";
            } else {
                _title = item._title;
            }
            viewHolder.tvTitle.setText(_title);
            viewHolder.tvYear.setText(item._year);
            return convertView;
        }

        @Override
        public void onClick(View v) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            if (v == viewHolder.btnViewMore) {
                ItemAssign itemAssign = getItem(viewHolder.position);
                if (viewHolder.btnViewMore.getText().equals("View More")) {
                    viewHolder.btnViewMore.setText("View Less");
                    viewHolder.tvTitle.setText(itemAssign._title);
                    viewHolder.llViewMore.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.btnViewMore.setText("View More");
                    viewHolder.llViewMore.setVisibility(View.GONE);
                    String _title = "";
                    if ((!itemAssign._title.isEmpty()) && (itemAssign._title.length() > 50)) {
                        _title = itemAssign._title.substring(0, 45) + "...";
                    } else {
                        _title = itemAssign._title;
                    }
                    viewHolder.tvTitle.setText(_title);
                }
            }
        }

        private class ViewHolder {
            TextView tvDay, tvMonth, tvYear, tvTitle;
            TextView tvDesc, tvClass, tvSub;
            Button btnViewMore;
            LinearLayout llViewMore;
            int position;
        }
    }

    private void initList(ArrayList<ItemAssign> list) {
        if ((list != null) && (list.size() > 0)) {
            lvAssign.setAdapter(new AdapterAssign(getContext(), 0, list));
            llNothingP.setVisibility(View.GONE);
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if ((asynGetAssign != null) && (asynGetAssign.getStatus() == AsyncTask.Status.RUNNING)) {
                asynGetAssign.cancel(true);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            asynGetAssign=new AsynGetAssign();
            asynGetAssign.execute();
        }catch (Exception e){

        }

    }
}
