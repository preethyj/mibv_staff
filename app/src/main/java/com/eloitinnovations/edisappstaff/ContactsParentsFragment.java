package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class ContactsParentsFragment extends ListFragment implements AdapterView.OnItemSelectedListener {

    private OnFragmentInteractionListener mListener;

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;
    private AsyncGetStudentList asyncGetStudentList;
    private AsyncClassList asyncClassList;

    private LinearLayout llNothingP;
    private ProgressBar pbTitle;
    private ArrayList<SpClassItems> listClass;
    private Spinner spClass, spStandard;
    private EditText ET_search;
    private TextView tvNothingP;

    private String _class_id;
    private String _staff_id;
    private String _academic_year_id;

    ArrayList<ItemList> list;
    ArrayList<ItemList> tempList =  new ArrayList<>();

    public ContactsParentsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ContactsParentsFragment newInstance() {
        ContactsParentsFragment fragment = new ContactsParentsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts_parents, container, false);
        spClass = (Spinner) view.findViewById(R.id.spCPClass);
        spStandard = (Spinner) view.findViewById(R.id.spCPStandard);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        ET_search = view.findViewById(R.id.et_contacts_parent_search);
        tvNothingP = (TextView) view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("Sorry. Your contacts have not been mapped…");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        pbTitle.setVisibility(View.GONE);

        spClass.setOnItemSelectedListener(this);
        spStandard.setOnItemSelectedListener(this);

        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemList itemList = (ItemList) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), SendMessageActivity.class);
                intent.putExtra(Config.INTENT_NAME, itemList._student_name);
                intent.putExtra(Config.INTENT_ID, itemList._msg_Id);
                intent.putExtra(Config.INTENT_CLASS_ID, _class_id);
                intent.putExtra(Config.INTENT_STUD_ID, itemList._student_id);
                startActivity(intent);
            }
        });

        if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncClassList.cancel(true);
        asyncClassList = new AsyncClassList();
        asyncClassList.execute();

        ET_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    if (list != null) {
                        setListAdapter(new AdapterList(getContext(), 0, list));
                        getListView().deferNotifyDataSetChanged();
                    }
                } else {
                    searchStudents(s.toString());
                }
            }
        });
    }

    private void searchStudents(String name) {
        if(name.isEmpty())
            return;

        if(list==null|| list.size()==0)
            return;

        tempList.clear();
        for(ItemList temp:list){
            if(temp._student_name.toLowerCase().contains(name.toLowerCase())||
                    temp._admn_no.toLowerCase().contains(name.toLowerCase()))
                tempList.add(temp);
        }

        setListAdapter(new AdapterList(getContext(),0,tempList));
        getListView().deferNotifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == spStandard.getId()) {
            SpClassItems spClassItems = (SpClassItems) parent.getItemAtPosition(position);
            _class_id = spClassItems._id;
            if ((asyncGetStudentList != null) && (asyncGetStudentList.getStatus().equals(AsyncTask.Status.RUNNING))) {
                asyncGetStudentList.cancel(true);
            }
            asyncGetStudentList = new AsyncGetStudentList();
            asyncGetStudentList.execute(_class_id, _academic_year_id, _staff_id);
        } else if (parent.getId() == spClass.getId()) {
            String _standard = (String) parent.getItemAtPosition(position);
            initSpStandard(listClass, _standard);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SpClassItems {
        String _name;
        String _id;
        String _standard;
    }

    private class ItemList {
        String _student_id;
        String _student_name;
        String _admn_no;
        String _msg_Id;
    }

    private class AsyncGetStudentList extends AsyncTask<String, Void, String> {


        ItemList itemList;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
            llNothingP.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(Config.URL + Config.URL_GET_STDNT_DETAIL_BY_CLASS + "?ClassId=" + params[0]
                        + "&AcademicYearId=" + params[1] + "&StaffId=" + params[2]);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    itemList = new ItemList();
                    itemList._student_id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("StudentId"));
                    itemList._admn_no = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("AdmissionNo"));
                    itemList._msg_Id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("MsgId"));
                    itemList._student_name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("StudentName"));
                    list.add(itemList);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }

            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                if ((list != null) && (list.size() > 0)) {
                    setListAdapter(new AdapterList(getContext(), 0, list));
                } else {
                    llNothingP.setVisibility(View.VISIBLE);
                }
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AsyncClassList extends AsyncTask<Void, Void, String> {

        String _requestURL;
        SpClassItems spClassItems;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);

            _requestURL = Config.URL + Config.URL_GET_STAFF_CLASS_MAP + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null) +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            listClass = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    spClassItems = new SpClassItems();
                    spClassItems._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                    spClassItems._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.NAME));
                    spClassItems._standard = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STANDARD));
                    listClass.add(spClassItems);
                }
                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    initSpClass(listClass);
                    break;
                case Config.ERROR:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterList extends ArrayAdapter<ItemList> {

        LayoutInflater inflater;
        ViewHolder viewholder;

        public AdapterList(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemList> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_contacts_parents, null);
                viewholder = new ViewHolder();
                viewholder.tvName = (TextView) convertView.findViewById(R.id.tvCPListName);
                viewholder.tvAdmNo = (TextView) convertView.findViewById(R.id.tvCPListAdmNo);
                convertView.setTag(viewholder);
            }
            if ((position % 2) == 0) {
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#fcfcfc"));
            }
            viewholder = (ViewHolder) convertView.getTag();
            ItemList itemList = getItem(position);
            viewholder.tvName.setText(itemList._student_name);
            viewholder.tvAdmNo.setText("Adm No: " + itemList._admn_no);
            return convertView;
        }

        private class ViewHolder {
            TextView tvName;
            TextView tvAdmNo;
        }
    }

    private class AdapterSPClass extends ArrayAdapter<SpClassItems> {

        LayoutInflater inflater;
        ViewHolder holder;

        public AdapterSPClass(@NonNull Context context, @LayoutRes int resource, @NonNull List<SpClassItems> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                holder.tvItem.setGravity(Gravity.LEFT);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        private class ViewHolder {
            TextView tvItem;
        }
    }

    private void initSpStandard(ArrayList<SpClassItems> list, String _Standard) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<SpClassItems> listByStand = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (spClassItems._standard.equals(_Standard)) {
                    listByStand.add(spClassItems);
                }
            }
            if ((listByStand != null) && (listByStand.size() > 0))
                spStandard.setAdapter(null);
            spStandard.setAdapter(new AdapterSPClass(getContext(), android.R.layout.simple_spinner_dropdown_item, listByStand));
        }
    }

    private void initSpClass(ArrayList<SpClassItems> list) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listStandard = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (!listStandard.contains(spClassItems._standard)) {
                    listStandard.add(spClassItems._standard);
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.sp_item, listStandard);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spClass.setAdapter(adapter);
        } else {
            Toast.makeText(getContext(), "Classes not mapped", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncClassList.cancel(true);
            if ((asyncGetStudentList != null) && (asyncGetStudentList.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetStudentList.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
