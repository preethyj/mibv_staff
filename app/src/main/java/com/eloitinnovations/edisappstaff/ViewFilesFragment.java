package com.eloitinnovations.edisappstaff;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewFilesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewFilesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewFilesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters

    private ArrayList<String> mParam1;
    ImageView ivLeftArrow,ivRightArrow,ivImage;
    TextView tvTitle;
    Button btnDownload;
    private int count=0;
    DownloadManager.Request request;

    private OnFragmentInteractionListener mListener;

    public ViewFilesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param urlList
     * @return A new instance of fragment ViewFilesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewFilesFragment newInstance(ArrayList<String> urlList) {
        ViewFilesFragment fragment = new ViewFilesFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, urlList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getStringArrayList(ARG_PARAM1);
        }
        //PRDownloader.initialize(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_files, container, false);
        ivLeftArrow = (ImageView) view.findViewById(R.id.ivArrowLeft);
        ivRightArrow = (ImageView) view.findViewById(R.id.ivArrowRight);
        ivImage = (ImageView) view.findViewById(R.id.ivPic);
        btnDownload = (Button) view.findViewById(R.id.btnDownload);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        tvTitle.setText("View Files");

        if(count==0) {
            imageLoad(count);
        }


        if(mParam1.size()==1)
        {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

            ivLeftArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count--;
                    if(count>=0) {
                        imageLoad(count);
                    }
                }
            });
            ivRightArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count++;
                    if(count<=mParam1.size()){
                        imageLoad(count);
                    }
                }
            });

    }

    private void imageLoad(int count) {
        final String url = mParam1.get(count);

        if(url.toLowerCase().contains(".pdf"))
        {
            ivImage.setImageResource(R.drawable.index);
            Toast.makeText(getContext(),"Document preview unavailable. Please download",Toast.LENGTH_SHORT).show();
            btnDownload.setText("Download");
        }else if((url.toLowerCase().contains(".jpg"))||(url.toLowerCase().contains(".png"))||(url.toLowerCase().contains(".jpeg")))
        {
            Picasso.get().load(url).error(R.drawable.index).into(ivImage);
            btnDownload.setText("Open File");
        }

        if(count==0) {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.VISIBLE);
        }else if(count==mParam1.size()-1) {
            ivRightArrow.setVisibility(View.GONE);
            ivLeftArrow.setVisibility(View.VISIBLE);
        }else
        {
            ivRightArrow.setVisibility(View.VISIBLE);
            ivLeftArrow.setVisibility(View.VISIBLE);
        }

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                getActivity().startActivity(i);

            }
        });
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
