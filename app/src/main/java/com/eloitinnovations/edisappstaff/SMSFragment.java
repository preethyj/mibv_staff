package com.eloitinnovations.edisappstaff;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class SMSFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private DbControl dbControl;
    private StaffControl staffControl;
    private SharedPreferences spLogin;
    private DbHelper dbHelper;
    private AsyncSMS asyncSMS;

    private ProgressBar pbTB;
    private ListView lvSms;
    private LinearLayout llNothingP;
    private TextView tvNothingP;

    private Dialog dialog;

    public SMSFragment() {
        // Required empty public constructor
    }


    public static SMSFragment newInstance() {
        SMSFragment fragment = new SMSFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms, container, false);
        pbTB = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        lvSms = (ListView) view.findViewById(R.id.lvSMSList);
        tvNothingP = (TextView)view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("No SMS notification yet");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dbControl = new DbControl();
        staffControl = new StaffControl();
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        dbHelper = new DbHelper(getContext());
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("SMS");
        btnHome.setVisibility(View.VISIBLE);
        pbTB.setVisibility(View.GONE);
        lvSms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    EachRow row = (EachRow) parent.getItemAtPosition(position);
                    dialog.setContentView(R.layout.dialog_sms);
                    TextView tvMsg = (TextView) dialog.findViewById(R.id.tvDialogSMSMsg);
                    TextView tvDate = (TextView) dialog.findViewById(R.id.tvDialogSMSDate);
                    tvDate.setText(row._date);
                    tvMsg.setText(row._msg);
                    try{
                        tvMsg.setMovementMethod(new ScrollingMovementMethod());
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        try {
            if ((asyncSMS != null) && (asyncSMS.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncSMS.cancel(true);
            asyncSMS = new AsyncSMS();
            asyncSMS.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_SMS_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class EachRow {
        String _date;
        String _msg;
    }

    private class SMSListAdater extends ArrayAdapter<EachRow> {

        LayoutInflater inflater;
        ViewHolder holder;

        public SMSListAdater(Context context, int resource, List<EachRow> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_sms, null);
                holder = new ViewHolder();
                holder.tvSMSDate = (TextView) convertView.findViewById(R.id.tvSMSDate);
                holder.tvSMSMsg = (TextView) convertView.findViewById(R.id.tvSMSmsg);
                convertView.setTag(holder);
            }
            if ((position % 2) == 0) {
                convertView.setBackgroundColor(Color.parseColor("#fcfcfc"));
            } else {
                convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            holder = (ViewHolder) convertView.getTag();
            EachRow row = getItem(position);
            String _msg;
            if (row._msg.length() > 50) {
                _msg = row._msg.substring(0, 45) + " ...";
            } else {
                _msg = row._msg;
            }
            holder.tvSMSDate.setText(staffControl.getNullStr(row._date));
            holder.tvSMSMsg.setText(staffControl.getNullStr(_msg));
            return convertView;
        }


        private class ViewHolder {
            TextView tvSMSDate;
            TextView tvSMSMsg;
        }
    }

    private void initListAdpter(ArrayList<EachRow> list) {
        if ((list != null) && (list.size() > 0)) {
            lvSms.setAdapter(new SMSListAdater(getContext(), 0, list));
            dbHelper.updateNotif(Config.NOTIF_SMS, list.size(), 0);
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    private class AsyncSMS extends AsyncTask<Void, Void, String> {

        String requestURL;
        EachRow row;
        ArrayList<EachRow> list;

        @Override
        protected void onPreExecute() {
            pbTB.setVisibility(View.VISIBLE);
            requestURL = Config.URL + Config.URL_GET_SMS_LIST + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null);
            list = new ArrayList<>();
        }


        @Override
        protected String doInBackground(Void... params) {
            String response = "ERROR";
            try {
                response = dbControl.sendGetRequest(requestURL);
                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    row = new EachRow();
                    row._date = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.DATE));
                    row._msg = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.CONTENT));
                    list.add(row);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                response = "ERROR";
            } catch (Exception e) {
                e.printStackTrace();
                response = "ERROR";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("ERROR")) {
                initListAdpter(list);
                pbTB.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncSMS != null) && (asyncSMS.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncSMS.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
