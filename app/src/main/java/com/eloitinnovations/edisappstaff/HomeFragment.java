package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class HomeFragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;

    private ImageButton btnAnnounce;
    private ImageButton btnNews;
    private ImageButton btnAttendance;
    private ImageButton btnLeave;
    private ImageButton btnSms;
    private ImageButton btnChats;
    private ImageButton btnTimeTable;
    private ImageButton btnStudentsList;
    private ImageButton btnAssignments;
    private ImageButton btnHomeWorks;
    private ImageButton btnHome;

    private TextView tvTitle;

    private ImageView ivNotifAnnouncement,ivNotifNews,ivNotifSms,ivNotifParentChat,ivNotifStaffChat,ivNotifLeave;

    private Thread tNotif;
    private SharedPreferences spLogin;
    private DbHelper dbHelper;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        btnAnnounce = (ImageButton) view.findViewById(R.id.btnHomeAnnounc);
        btnNews = (ImageButton) view.findViewById(R.id.btnHomeNews);
        btnAttendance = (ImageButton) view.findViewById(R.id.btnHomeAttendance);
        btnLeave = (ImageButton) view.findViewById(R.id.btnHomeLeave);
        btnSms = (ImageButton) view.findViewById(R.id.btnHomeSms);
        btnTimeTable = (ImageButton) view.findViewById(R.id.btnHomeTimeTable);
        btnChats = (ImageButton) view.findViewById(R.id.btnHomeMessages);
        btnStudentsList = (ImageButton) view.findViewById(R.id.btnHomeStudents);
        btnAssignments = (ImageButton) view.findViewById(R.id.btnHomeAssignments);
        btnHomeWorks = (ImageButton) view.findViewById(R.id.btnHomeHomeWorks);
        ivNotifAnnouncement=(ImageView) view.findViewById(R.id.ivNotifAnnounc);
        ivNotifNews=(ImageView) view.findViewById(R.id.ivNotifNews);
        ivNotifLeave=(ImageView) view.findViewById(R.id.ivNotifLeave);
        ivNotifSms=(ImageView) view.findViewById(R.id.ivNotifSms);
        ivNotifParentChat=(ImageView) view.findViewById(R.id.ivNotifParentChat);
        ivNotifStaffChat=(ImageView) view.findViewById(R.id.ivNotifStaffChat);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnAnnounce.setOnClickListener(this);
        btnNews.setOnClickListener(this);
        btnAttendance.setOnClickListener(this);
        btnLeave.setOnClickListener(this);
        btnSms.setOnClickListener(this);
        btnTimeTable.setOnClickListener(this);
        btnChats.setOnClickListener(this);
        btnStudentsList.setOnClickListener(this);
        btnAssignments.setOnClickListener(this);
        btnHomeWorks.setOnClickListener(this);

        tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Home");
        btnHome.setVisibility(View.GONE);
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == btnAnnounce) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    AnnouncementFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnNews) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    NewsFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnAttendance) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    AttendanceFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnSms) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    SMSFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnLeave) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    LeaveFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnTimeTable) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    TimeTableManinFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnChats) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    ChatsStaffMainFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnStudentsList) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    ChatsParentMainFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnAssignments) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    AssignmentFragment.newInstance()).addToBackStack(null).commit();
        } else if (v == btnHomeWorks) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flHomeActivity,
                    HomeworkFragment.newInstance()).addToBackStack(null).commit();
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
