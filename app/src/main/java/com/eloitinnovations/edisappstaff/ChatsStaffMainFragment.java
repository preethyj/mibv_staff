package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;


public class ChatsStaffMainFragment extends Fragment {

    private OnFragmentInteractionListener mListener;


    private ProgressBar pbTitle;
    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;
    private DbHelper dbHelper;
    private AsyncGetMessages asyncGetMessages;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public ChatsStaffMainFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChatsStaffMainFragment newInstance() {
        ChatsStaffMainFragment fragment = new ChatsStaffMainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_staff_main, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutMessages);
        viewPager = (ViewPager) view.findViewById(R.id.pagerMessages);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        dbHelper = new DbHelper(getContext());
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Messages");
        btnHome.setVisibility(View.VISIBLE);
        tabLayout.addTab(tabLayout.newTab().setText("Inbox"));
        tabLayout.addTab(tabLayout.newTab().setText("Outbox"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        if ((asyncGetMessages != null) && (asyncGetMessages.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncGetMessages.cancel(true);
        asyncGetMessages = new AsyncGetMessages();
        asyncGetMessages.execute(spLogin.getString(Config.ID, null));
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Cht_Stff_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncGetMessages extends AsyncTask<String, Void, String> {

        ArrayList<ItemStaffMessages> listInbox;
        ArrayList<ItemStaffMessages> listOutbox;
        ItemStaffMessages itemMessages;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            listInbox = new ArrayList<>();
            listOutbox = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                JSONArray jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_STAFF_COMNCTN_MSG_API + "?StaffId=" + params[0]));
                for (int i = 0; i < jsonArray.length(); i++) {
                    itemMessages = new ItemStaffMessages();
                    itemMessages._staff_name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("StaffName"));
                    itemMessages._subject = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Subject"));
                    itemMessages._content = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Content"));
                    String _date = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Date"));
                    Calendar calendar = staffControl.getDateByString(_date);
                    itemMessages._date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                    if (staffControl.getNullStr(jsonArray.getJSONObject(i).getString("FromStaffId")).equals(params[0])) {
                        listOutbox.add(itemMessages);
                    } else {
                        listInbox.add(itemMessages);
                    }
                }
                return Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                return Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initPager(listOutbox, listInbox);
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }

            pbTitle.setVisibility(View.GONE);
        }
    }

    private void initPager(ArrayList<ItemStaffMessages> listOutbox, ArrayList<ItemStaffMessages> listInbox) {
        PagerChatsStaff pagerMessages = new PagerChatsStaff(getActivity().getSupportFragmentManager(), tabLayout.getTabCount()
                , listOutbox, listInbox);
        if (listInbox != null) {
            dbHelper.updateNotif(Config.NOTIF_STAFF_CHATS, listInbox.size(), 0);
        }

        viewPager.setAdapter(pagerMessages);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.removeAllTabs();

        tabLayout.addTab(tabLayout.newTab().setText("Inbox"));
        tabLayout.addTab(tabLayout.newTab().setText("Outbox"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncGetMessages != null) && (asyncGetMessages.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetMessages.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
