package com.eloitinnovations.edisappstaff;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class LoginActivity extends AppCompatActivity {

    private DbControl dbControl = new DbControl();
    private DbHelper dbHelper;
    private StaffControl staffControl = new StaffControl();

    private EditText etUsername, etPassword;
    private Button btnLogin, btnDialogLogin;
    private TextView tvResponse;
    private ProgressBar pbLogin;
    private Dialog dialog;
    private AsyncLogin asyncLogin;
    private SharedPreferences spLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DbHelper(this);
        dbHelper.loginDeleteTables();
        setContentView(R.layout.activity_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        spLogin.edit().clear();
        spLogin.edit().apply();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setContentView(R.layout.dialog_login);
                btnDialogLogin = (Button) dialog.findViewById(R.id.btnDialogSignIn);
                etUsername = (EditText) dialog.findViewById(R.id.etDialogLoginUsername);
                etPassword = (EditText) dialog.findViewById(R.id.etDialogLoginPassword);
                tvResponse = (TextView) dialog.findViewById(R.id.tvDialogLoginRes);
                pbLogin = (ProgressBar) dialog.findViewById(R.id.pbDialogLogin);
                pbLogin.setVisibility(View.GONE);
                btnDialogLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(etUsername.getText().toString().isEmpty())
                        {
                            etUsername.setError("Email can't be empty");
                        }else if(etPassword.getText().toString().isEmpty()){
                            etPassword.setError("Password can't be empty");
                        }else{

                            try {
                                if (asyncLogin == null) {
                                    asyncLogin = new AsyncLogin();
                                }

                                if (asyncLogin.getStatus() != AsyncTask.Status.RUNNING) {
                                    asyncLogin = new AsyncLogin();
                                    asyncLogin.execute();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                dialog.show();
            }
        });
        dbHelper.loginDeleteTables();
    }

    private class AsyncLogin extends AsyncTask<Void, Void, String> {

        String _requestURL;
        String _username;
        String _password;
        String _id;
        String _academic_year_id;
        String _email;
        String _crnt_addr;
        String _mobile;
        String _gender;
        String _appnmnt_date;
        String _cnfrmtn_date;
        String _addrs;
        String _mrtal_status;
        String _desg;
        String _dob;
        String _name;
        String _class_id;
        String _class_name;
        String _emergency_contact;

        @Override
        protected void onPreExecute() {
            btnDialogLogin.setVisibility(View.GONE);
            pbLogin.setVisibility(View.VISIBLE);
            tvResponse.setVisibility(View.INVISIBLE);
            tvResponse.setText("");
            _username = etUsername.getText().toString();
            _password = etPassword.getText().toString();
            _username.replaceAll(" ", "");
            _password.replaceAll(" ", "");
            _requestURL = Config.URL + Config.URL_STAFF_LOGIN + Config.USERNAME + "=" + _username
                    + "&" + Config.PASSWORD + "=" + _password;
          //  System.out.println("-->request URL "+_requestURL);
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;

            try {
                _response = dbControl.sendGetRequest(_requestURL);

                JSONObject jsonLoginRes = new JSONObject(_response);
            //    System.out.println("-->response "+jsonLoginRes);
                if (!jsonLoginRes.getString(Config.LOGIN_MESSAGE).equals(Config.INVALID_LOGIN)) {
                    JSONObject jsonResult = jsonLoginRes.getJSONObject(Config.RESULT);
                    if (!(jsonResult.getString(Config.ID).equals("0") || jsonResult.getString(Config.ID).isEmpty())) {
                        _id = jsonResult.getString(Config.ID);
                        _academic_year_id = jsonResult.getString(Config.ACADEMIC_YEAR_ID);
                        _email = staffControl.getNullStr(jsonResult.getString(Config.EMAIL));
                        _crnt_addr = staffControl.getNullStr(jsonResult.getString(Config.CURRENT_ADDRESS));
                        _mobile = staffControl.getNullStr(jsonResult.getString(Config.MOBILE));
                        _gender = staffControl.getNullStr(jsonResult.getString(Config.GENDER));
                        _appnmnt_date = staffControl.getNullStr(jsonResult.getString(Config.APPOINMENT_DATE));
                        _cnfrmtn_date = staffControl.getNullStr(jsonResult.getString(Config.CONFIRMATION_DATE));
                        _addrs = staffControl.getNullStr(jsonResult.getString(Config.ADDRESS));
                        _mrtal_status = staffControl.getNullStr(jsonResult.getString(Config.MARITAL_STATUS));
                        _desg = staffControl.getNullStr(jsonResult.getString(Config.DESIGNATION));
                        _dob = staffControl.getNullStr(jsonResult.getString(Config.DATE_OF_BIRTH));
                        _name = staffControl.getNullStr(jsonResult.getString(Config.NAME));
                        _class_id = staffControl.getNullStr(jsonResult.getString(Config.CLASS_ID));
                        _emergency_contact = staffControl.getNullStr(jsonResult.getString(Config.EMERGENCY_CONTACT));
                        if ((_class_id != null) && (!_class_id.isEmpty()) && (!_class_id.equals("0"))) {
                            _class_name = staffControl.getNullStr(jsonResult.getString(Config.CLASS_NAME));
                        } else {
                            _class_name = "";
                        }

                        if ((_appnmnt_date != null) && (!_appnmnt_date.equals("")) && (!_appnmnt_date.isEmpty())) {
                            Calendar calendar = staffControl.getDateByString(_appnmnt_date);
                            _appnmnt_date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                        }
                        if ((_cnfrmtn_date != null) && (!_cnfrmtn_date.equals("")) && (!_cnfrmtn_date.isEmpty())) {
                            Calendar calendar = staffControl.getDateByString(_cnfrmtn_date);
                            _cnfrmtn_date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                        }
                        if ((_dob != null) && (!_dob.equals("")) && (!_dob.isEmpty())) {
                            Calendar calendar = staffControl.getDateByString(_dob);
                            _dob = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                        }
                        _response = Config.SUCCESS;
                    } else {
                        _response = Config.INVALID_LOGIN;
                    }
                } else {
                    _response = Config.INVALID_LOGIN;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    SharedPreferences.Editor spEditor = spLogin.edit();
                    spEditor.putString(Config.ID, _id);
                    spEditor.putString(Config.ACADEMIC_YEAR_ID, _academic_year_id);
                    spEditor.putString(Config.EMAIL, _email);
                    spEditor.putString(Config.CURRENT_ADDRESS, _crnt_addr);
                    spEditor.putString(Config.MOBILE, _mobile);
                    spEditor.putString(Config.GENDER, _gender);
                    spEditor.putString(Config.APPOINMENT_DATE, _appnmnt_date);
                    spEditor.putString(Config.CONFIRMATION_DATE, _cnfrmtn_date);
                    spEditor.putString(Config.ADDRESS, _addrs);
                    spEditor.putString(Config.MARITAL_STATUS, _mrtal_status);
                    spEditor.putString(Config.DESIGNATION, _desg);
                    spEditor.putString(Config.DATE_OF_BIRTH, _dob);
                    spEditor.putString(Config.NAME, _name);
                    spEditor.putString(Config.CLASS_ID, _class_id);
                    spEditor.putString(Config.CLASS_NAME, _class_name);
                    spEditor.putString(Config.EMERGENCY_CONTACT, _emergency_contact);
                    spEditor.commit();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                    break;
                case Config.INVALID_LOGIN:
                    tvResponse.setVisibility(View.VISIBLE);
                    tvResponse.setText(Config.INVALID_CREDITIONLS);
                    break;
                case Config.ERROR:
                    tvResponse.setVisibility(View.VISIBLE);
                    tvResponse.setText(Config.NETWORK_ERROR);
                    break;
            }
            try {

                pbLogin.setVisibility(View.GONE);
                btnDialogLogin.setVisibility(View.VISIBLE);

            }catch (Exception e){

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (asyncLogin != null) {
                if (asyncLogin.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncLogin.cancel(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
