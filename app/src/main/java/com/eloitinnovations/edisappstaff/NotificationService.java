package com.eloitinnovations.edisappstaff;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class NotificationService extends Service {

    private NotificationManager manager;
    private Notification.Builder builder;
    private Notification notification;

    private DbHelper dbHelper;
    private DbControl dbControl;
    private StaffControl staffControl;
    private Boolean isRun;
    private SharedPreferences spLogin;
    private Calendar calendar;

    private String _staff_id, _academic_id;
    String Channel_ID = "Edisapp_S_mivbvtS";

    public NotificationService() {
        dbHelper = new DbHelper(this);
        dbControl = new DbControl();
        staffControl = new StaffControl();
        isRun = true;

    }

 //   int i=0;

    @Override
    public void onCreate() {
        super.onCreate();
        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
        calendar = Calendar.getInstance();
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Create the channel object with the unique ID FOLLOWERS_CHANNEL.
        NotificationChannel Channel = null;
        /*int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = null;
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            manager.createNotificationChannel(notificationChannel);
        }*/
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder = new Notification.Builder(getApplicationContext());
        Intent notifIntent = new Intent(this.getApplicationContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, notifIntent, 0);
        /////////////////
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Channel = new NotificationChannel(
                    Channel_ID,
                    "Edisapp Staff App",
                    NotificationManager. IMPORTANCE_HIGH);
            // Configure the channel's initial settings.
            Channel.setLightColor(Color.GREEN);
            Channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 500, 200, 500});
            // Submit the notification channel object to the notification manager.
            manager.createNotificationChannel(Channel);
        }
        builder = getNotificationFollower();
        builder.setSound(alarmSound);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(getResources().getString(R.string.app_name));
       /* builder.setSmallIcon(android.R.drawable.stat_notify_chat);
        builder.setSound(alarmSound);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(getResources().getString(R.string.app_name));*/
      //  if(!((_staff_id!=null)||(_staff_id.equals("")||(_staff_id.equals(" "))||(_staff_id.equals("0")))))

            try {
             //   i=100;
                 new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (isRun) {
                 //          System.out.println("sssssssssssssssssssssssss "+(i++));
                            try {
    //                           Thread.sleep(15000);
///*
                                if (spLogin.getAll().size() == 16) {
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_ANNOUNC);
                                    //2400000
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_NEWS);
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_SMS);
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_LEAVE);
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_PARENT_CHAT);
                                    Thread.sleep(2400000);
                                    new AsyncArray().execute(Config.NOTIF_STAFF_CHATS);
                                }
                              //  */

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    private class AsyncArray extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected Integer doInBackground(Integer... params) {
            try {
                int returnVal;
                JSONArray jsonArray;
                switch (params[0]) {
                    case Config.NOTIF_ANNOUNC:
                        jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_ANNOUNCEMENT_LIST + Config.ACADEMIC_YEAR_ID + "=" + _academic_id));
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_ANNOUNC)[0] < jsonArray.length()) ? Config.NOTIF_ANNOUNC : -1);
                        dbHelper.updateNotif(Config.NOTIF_ANNOUNC, jsonArray.length(), 1);
                        break;
                    case Config.NOTIF_NEWS:
                        jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_GET_ALL_NEWS_AND_EVENTS + Config.ACADEMIC_YEAR_ID + "=" + _academic_id));
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_NEWS)[0] < jsonArray.length()) ? Config.NOTIF_NEWS : -1);
                        dbHelper.updateNotif(Config.NOTIF_NEWS, jsonArray.length(), 1);
                        break;
                    case Config.NOTIF_LEAVE:
                        JSONArray jsonLeaveCat = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_STUDENT_LEAVE + "?" + Config.STAFF_ID + "=" + _staff_id + "&" + Config.ACADEMIC_YEAR_ID + "=" + _academic_id));
                        int count = 0;
                        for (int i = 0; i < jsonLeaveCat.length(); i++) {
                            JSONObject jsonCat = jsonLeaveCat.getJSONObject(i);
                            for (int j = 0; j < jsonCat.getJSONArray("leavemodel").length(); j++) {
                                count++;
                            }
                        }
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_LEAVE)[0] < count) ? Config.NOTIF_LEAVE : -1);
                        dbHelper.updateNotif(Config.NOTIF_LEAVE, count, 1);
                        break;
                    case Config.NOTIF_SMS:
                        jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_GET_SMS_LIST + Config.STAFF_ID + "=" + _staff_id));
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_SMS)[0] < jsonArray.length()) ? Config.NOTIF_SMS : -1);
                        dbHelper.updateNotif(Config.NOTIF_SMS, jsonArray.length(), 1);
                        break;
                    case Config.NOTIF_STAFF_CHATS:
                        jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_STAFF_COMNCTN_MSG_API + "?StaffId=" + _staff_id));
                        int countInbox = 0;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (!staffControl.getNullStr(jsonArray.getJSONObject(i).getString("FromStaffId")).equals(_staff_id))
                                countInbox++;
                        }
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_STAFF_CHATS)[0] < countInbox) ? Config.NOTIF_STAFF_CHATS : -1);
                        dbHelper.updateNotif(Config.NOTIF_STAFF_CHATS, countInbox, 1);
                        break;
                    case Config.NOTIF_PARENT_CHAT:
                        JSONObject jsonObject = new JSONObject(dbControl.sendGetRequest(Config.URL + Config.URL_PARENT_CMNCTN_API
                                + "?StaffId=" + _staff_id + "&AcademicYearId=" + _academic_id));
                        jsonArray = jsonObject.getJSONArray("Notification");
                        returnVal = ((dbHelper.getNotifVal(Config.NOTIF_PARENT_CHAT)[0] < jsonArray.length() ? Config.NOTIF_PARENT_CHAT : -1));
                        dbHelper.updateNotif(Config.NOTIF_PARENT_CHAT, jsonArray.length(), 1);
                        break;
                    default:
                        return -1;
                }
               // System.out.println("-->ss returnVal in return"+returnVal);
                return returnVal;
            } catch (JSONException e) {
                e.printStackTrace();
                return -1;
            } catch (Exception e) {
                e.printStackTrace();
               // System.out.println("Exception e "+ e);
                return -1;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (integer != -1) {
                switch (integer) {
                    case Config.NOTIF_ANNOUNC:
                        builder.setContentText("New Announcement Received");
                        break;
                    case Config.NOTIF_NEWS:
                        builder.setContentText("New News Received");
                        break;
                    case Config.NOTIF_LEAVE:
                        builder.setContentText("New Leave Request Received");
                        break;
                    case Config.NOTIF_SMS:
                        builder.setContentText("New SMS Received");
                        break;
                    case Config.NOTIF_PARENT_CHAT:
                        builder.setContentText("New Message From Parent");
                        break;
                    case Config.NOTIF_STAFF_CHATS:
                        builder.setContentText("New Inbox From Staff");
                        break;
                }
                builder.setWhen(calendar.getTimeInMillis());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    builder.setShowWhen(true);
                }
                notification = builder.build();
                manager.notify(1, notification);
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////
    public Notification.Builder getNotificationFollower() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new Notification.Builder(getApplicationContext(), Channel_ID)
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setAutoCancel(true)
                    .setContentTitle(getResources().getString(R.string.app_name));
        }else{
            return  new Notification.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setAutoCancel(true)
                    .setContentTitle(getResources().getString(R.string.app_name));
        }
    }
/////////////////////////////////////////////////////////////////////////////

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isRun = true;
        //System.out.println("sssssssssssssssssss oncommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        isRun = true;//System.out.println("sssssssssssssssssss onbind");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        isRun = false;//System.out.println("sssssssssssssssssss onunbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();//System.out.println("sssssssssssssssssss ondestroy");
        isRun = false;
    }
}
