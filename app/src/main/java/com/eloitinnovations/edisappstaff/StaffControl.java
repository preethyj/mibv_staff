package com.eloitinnovations.edisappstaff;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 28-03-2017.
 */

public class StaffControl {

    public static String getNullStr(String _item) {
        if ((_item==null)||(_item.isEmpty()) || (_item.equals("null"))) {
            return "";
        } else {
            return _item;
        }
    }
    public String getDayByInt(int position){
        switch(position){
            case 0:
                return "Mon";
            case 1:
                return "Tue";
            case 2:
                return "Wed";
            case 3:
                return "Thu";
            case 4:
                return "Fri";
            case 5:
                return "Sat";
            default:
                return "";
        }
    }
    public String getMonthById(int month){
        switch (month){
            case 0:
                return "JAN";
            case 1:
                return "FEB";
            case 2:
                return "MAR";
            case 3:
                return "APR";
            case 4:
                return "MAY";
            case 5:
                return "JUN";
            case 6:
                return "JUL";
            case 7:
                return "AUG";
            case 8:
                return "SEP";
            case 9:
                return "OCT";
            case 10:
                return "NOV";
            case 11:
                return "DEC";
            default:
                return "";
        }
    }

    public Calendar getDateByString(String _date){
        _date = _date.replace("/Date(","").replace(")/","");
        Long aLong = Long.valueOf(_date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(aLong);
        return calendar;
    }
}
