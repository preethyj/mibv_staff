package com.eloitinnovations.edisappstaff;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class StaffSendActivity extends AppCompatActivity {

    private TextView tvDate,tvTo;
    private TextView tvTitle;
    private ImageButton btnHome;
    private EditText etSubject,etMsg;
    private Button btnSend;
    private ProgressBar pbTitle;
    private String _to_staff_id,_staff_name;
    private DbControl dbControl= new DbControl();
    private SharedPreferences spLogin;
    private AsyncSendMsg asyncSendMsg;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);
        pbTitle = (ProgressBar) toolbar.findViewById(R.id.pbTopBarCont);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        tvTitle.setText("Message");
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTo = (TextView)findViewById(R.id.tvStaffSendTo);
        tvDate= (TextView) findViewById(R.id.tvStaffSendDate);
        etSubject =(EditText) findViewById(R.id.etStaffSendSubject);
        etMsg = (EditText) findViewById(R.id.etStaffSendMessage);
        btnSend = (Button) findViewById(R.id.btnStaffSendSend);
        spLogin = getSharedPreferences(Config.SHR_LOGIN,MODE_PRIVATE);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
  //              try{

                    if((asyncSendMsg!=null)&&(asyncSendMsg.getStatus().equals(AsyncTask.Status.RUNNING)))
                        asyncSendMsg.cancel(true);
                    asyncSendMsg = new AsyncSendMsg();
                    asyncSendMsg.execute(etSubject.getText().toString(),etMsg.getText().toString(),_to_staff_id,spLogin.getString(Config.ID,null));
    //            }catch (Exception e){
      //              e.printStackTrace();
        //            Toast.makeText(StaffSendActivity.this, "Error in fetching Data", Toast.LENGTH_SHORT).show();
          //      }

            }
        });
        try{
            _to_staff_id = getIntent().getStringExtra("TO_STAFF_ID");
            _staff_name = getIntent().getStringExtra("STAFF_NAME");
            if(!_to_staff_id.isEmpty()&&!_staff_name.isEmpty()){
                Calendar calendar = Calendar.getInstance();
                tvDate.setText((String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)+"/"+(calendar.get(Calendar.MONTH)+1)
                        +"/"+calendar.get(Calendar.YEAR))));
                tvTo.setText(_staff_name);
            }else{
                onBackPressed();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
           // System.out.print("------>error<------"+e);
            onBackPressed();
        }
    }

    private class AsyncSendMsg extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            btnSend.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... params) {
            return dbControl.sendMessgeToStaff(params[0],params[1],params[2],params[3]);
        }

        @Override
        protected void onPostExecute(String s) {
            btnSend.setEnabled(true);
            if (s.equals(Config.SUCCESS)) {
                etSubject.setText("");
                etMsg.setText("");
                Toast.makeText(StaffSendActivity.this, "Message Send Successfully", Toast.LENGTH_LONG).show();
                onBackPressed();
            } else {
                Toast.makeText(StaffSendActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
               // System.out.println("--------->Error in onPostExwcute<-------");
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            if((asyncSendMsg!=null)&&(asyncSendMsg.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncSendMsg.cancel(true);
       }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(StaffSendActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(StaffSendActivity.this, NotificationService.class));
    }
}
