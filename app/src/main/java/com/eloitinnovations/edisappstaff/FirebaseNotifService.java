package com.eloitinnovations.edisappstaff;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class FirebaseNotifService extends FirebaseMessagingService {

    String FOLLOWERS_CHANNEL = "notification_channel_mivbvtS";
    Notification.Builder builder;
    NotificationChannel followersChannel;
    NotificationManager notificationManager;
    SharedPreferences spUser;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
       /* if(validateData(remoteMessage.getNotification().getBody())){
            sendNotification();
        }*/
        Map<String, String> data = remoteMessage.getData();
        if (data != null) {
            validateData(data);
        }

    }

    public void sendNotification(String notificationBody) {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Create the channel object with the unique ID FOLLOWERS_CHANNEL.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            followersChannel = new NotificationChannel(
                    FOLLOWERS_CHANNEL,
                    "mivbvtS",
                    NotificationManager.IMPORTANCE_HIGH);
            // Configure the channel's initial settings.
            followersChannel.setLightColor(Color.GREEN);
            followersChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 500, 200, 500});
            // Submit the notification channel object to the notification manager.
            notificationManager.createNotificationChannel(followersChannel);

        }
        String notificationHead = getResources().getString(R.string.app_name);
        Notification notification = getNotificationFollower(notificationHead, notificationBody).build();
        notificationManager.notify(1, notification);
    }

    public Notification.Builder getNotificationFollower(String title, String body) {
        Intent notificationIntent = new Intent(getApplicationContext(), SplashScreenActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new Notification.Builder(getApplicationContext(), FOLLOWERS_CHANNEL)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setAutoCancel(true)
                    .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 1, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            return new Notification.Builder(getApplicationContext()).setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 1, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        }
    }

    public boolean validateData(Map<String, String> data) {
        try {
            spUser = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
            String _academicYrId = spUser.getString(Config.ACADEMIC_YEAR_ID, "");
            String _staffId = spUser.getString(Config.ID, "");
            String _staff_name = spUser.getString(Config.NAME, "Edisapp");
            String _class_id = spUser.getString(Config.CLASS_ID, "");
            switch (data.get("TYPE")) {
                case "N_S_STAFF_ANNOUNCE":
                    if (data.get("ACDCYRID") == null)
                        break;
                    if(data.get("ACDCYRID").equals(_academicYrId))
                        sendNotification("New Announcement Received - " + _staff_name);
                    break;
                case "N_ANNOUNC":
                    try {
                        if (data.get("ACDCYRID") == null)
                            break;
                        Gson gson = new GsonBuilder().create();
                        AnnouncementFirebaseModel model = gson.fromJson(data.get("ACDCYRID"), AnnouncementFirebaseModel.class);
                        if (model == null)
                            break;
                        if (String.valueOf(model.id).equals(_academicYrId))
                            sendNotification("New Announcement Received - " + _staff_name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "N_NEWS":
                    if (data.get("ACDCYRID") != null && data.get("ACDCYRID").equalsIgnoreCase(_academicYrId))
                        sendNotification("New News Received - " + _staff_name);
                    break;
                case "N_EVENTS":
                    if (data.get("ACDCYRID") != null && data.get("ACDCYRID").equalsIgnoreCase(_academicYrId))
                        sendNotification("New Event Created - " + _staff_name);
                    break;
                case "N_SMS":
                    //   notificationBody = "New SMS Received";
                    break;
                case "N_S_LEAVE":
                    if ((data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("staff")) && (data.get("STAFFID") != null && data.get("STAFFID").equalsIgnoreCase(_staffId)))
                        sendNotification("New Leave Notification - " + _staff_name);
                    break;

                case "N_HOMEWORK":
                    if (data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("class")) {
                        if (data.get("CLASSID") != null && data.get("CLASSID").equalsIgnoreCase(_class_id))
                            sendNotification("New Homework Notification - " + _staff_name);
                    } else if (data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("staff")) {
                        if (data.get("STAFFID") != null && data.get("STAFFID").equalsIgnoreCase(_staffId))
                            sendNotification("New Homework Notification - " + _staff_name);
                    }
                    break;
                case "N_ASSIGNMENT":
                    if (data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("class")) {
                        if (data.get("CLASSID") != null && data.get("CLASSID").equalsIgnoreCase(_class_id))
                            sendNotification("New Assignment Notification - " + _staff_name);
                    } else if (data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("staff")) {
                        if (data.get("STAFFID") != null && data.get("STAFFID").equalsIgnoreCase(_staffId))
                            sendNotification("New Assignment Notification - " + _staff_name);
                    }
                    break;
                case "N_S_STAFF_CHAT":
                    if (data.get("MSG") != null && data.get("MSG").equalsIgnoreCase("staff")) {
                        if (data.get("STAFFID") != null && data.get("STAFFID").equalsIgnoreCase(_staffId))
                            sendNotification("New Chat Notification - " + _staff_name);
                    }
                    break;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

