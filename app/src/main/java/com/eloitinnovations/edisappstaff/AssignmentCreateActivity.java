package com.eloitinnovations.edisappstaff;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AssignmentCreateActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;
    private ArrayList<SpClassItems> listClass = new ArrayList<>();
    private ArrayList<SpSubjectItems> listSubjects = new ArrayList<>();

    private SimpleDateFormat simpleDateFormat;
    private DatePickerDialog dialogLastDate;

    private Toolbar toolbar;
    private ProgressBar pbTitle;
    private TextView tvTitle,tvDateError,tvMarkError;
    private ImageButton btnHome;

    private Spinner spSubjects, spStandard, spClass;
    private EditText etAssignTitle, etAssignDesc, etAssignLastDate, etAssignTotalMark;
    private Button btnSubmit;
    private CheckBox cbIsSms;

    private String _class_id;
    private String _subject_id;
    private String _staff_id;
    private String _academic_year_id;

    private AsyncClassList asyncClassList;
    private AsyncGetSubjects asyncGetSubjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_create);
        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        pbTitle = (ProgressBar) findViewById(R.id.pbFragmentSub);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);
        spStandard = (Spinner) findViewById(R.id.spAssignStandard);
        spClass = (Spinner) findViewById(R.id.spAssignClass);
        spSubjects = (Spinner) findViewById(R.id.spAssignSubjects);
        etAssignTitle = (EditText) findViewById(R.id.etAssignTitle);
        etAssignDesc = (EditText) findViewById(R.id.etAssignDesc);
        etAssignLastDate = (EditText) findViewById(R.id.etAssignLastDate);
        etAssignTotalMark = (EditText) findViewById(R.id.etAssignTotalMark);
        btnSubmit = (Button) findViewById(R.id.btnAssignSubmit);
        cbIsSms = (CheckBox) findViewById(R.id.cbAssignCreateSMS);
        tvDateError=(TextView) findViewById(R.id.tvLastDateError);
        tvMarkError=(TextView) findViewById(R.id.tvTotalMarkError);

        pbTitle.setVisibility(View.GONE);
        btnHome.setVisibility(View.GONE);
        tvTitle.setText("Create Assignment");
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AssignmentCreateActivity.this, HomeActivity.class));
                finish();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listClass.size()>0) {
                if(etAssignTotalMark.getText().toString().isEmpty()&& etAssignLastDate.getText().toString().isEmpty()) {
                    tvMarkError.setVisibility(View.VISIBLE);
                    tvDateError.setVisibility(View.VISIBLE);
                }else if(etAssignLastDate.getText().toString().isEmpty()&& (!etAssignTotalMark.getText().toString().isEmpty()))
                {
                    tvDateError.setVisibility(View.VISIBLE);
                    tvMarkError.setVisibility(View.GONE);
                }
                else if((!etAssignLastDate.getText().toString().isEmpty())&& etAssignTotalMark.getText().toString().isEmpty())
                {
                    tvDateError.setVisibility(View.GONE);
                    tvMarkError.setVisibility(View.VISIBLE);
                } else if(etAssignTitle.getText().toString().isEmpty()) {
                    etAssignTitle.setError("Title can't be empty");
                }
                else if(etAssignDesc.getText().toString().isEmpty())
                {
                    etAssignDesc.setError("Description can't be empty");
                }
                else {
                    new AsyncAssignSubmit().execute();
                }}else
                    {
                        Toast.makeText(AssignmentCreateActivity.this,"Assignment can't be created without class and subject mapping",Toast.LENGTH_SHORT).show();
                    }
            }
        });
        spSubjects.setOnItemSelectedListener(this);
        spClass.setOnItemSelectedListener(this);
        spStandard.setOnItemSelectedListener(this);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        Calendar calendar = Calendar.getInstance();
        dialogLastDate = new DatePickerDialog(AssignmentCreateActivity.this, R.style.DateDialog, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etAssignLastDate.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        etAssignLastDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLastDate.show();
            }
        });

        if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncClassList.cancel(true);
        asyncClassList = new AsyncClassList();
        asyncClassList.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Assign_Creat_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == spStandard.getId()) {
            SpClassItems spClassItems = (SpClassItems) parent.getItemAtPosition(position);
            _class_id = spClassItems._id;
            new AsyncGetSubjects().execute(spClassItems._id);
        } else if (parent.getId() == spClass.getId()) {
            String _standard = (String) parent.getItemAtPosition(position);
            initSpStandard(listClass, _standard);
        } else if (parent.getId() == spSubjects.getId()) {
            String _subject_name = (String) parent.getItemAtPosition(position);
            for (SpSubjectItems items : listSubjects) {
                if (items._name.equals(_subject_name))
                    _subject_id = items._id;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class SpSubjectItems {
        String _id;
        String _name;
    }

    private class SpClassItems {
        String _name;
        String _id;
        String _standard;
    }

    private class AsyncAssignSubmit extends AsyncTask<Void, Void, String> {

        String _title;
        String _last_date;
        String _desc;
        String _total_mark;
        String _isSms;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _title = etAssignTitle.getText().toString();
            _last_date = etAssignLastDate.getText().toString();
            _desc = etAssignDesc.getText().toString();
            _total_mark = etAssignTotalMark.getText().toString();
            _isSms = String.valueOf(cbIsSms.isChecked());
            btnSubmit.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... params) {
            return dbControl.createAssignment(_class_id, _subject_id, _staff_id, _last_date, _title, _desc, _total_mark, _academic_year_id, _isSms);
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                Toast.makeText(AssignmentCreateActivity.this, "Assignment Submitted Successfully", Toast.LENGTH_SHORT).show();
                onBackPressed();
            } else {
                Toast.makeText(AssignmentCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
            btnSubmit.setEnabled(true);
        }
    }

    private class AsyncClassList extends AsyncTask<Void, Void, String> {

        String _requestURL;
        SpClassItems spClassItems;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);

            _requestURL = Config.URL + Config.URL_GET_STAFF_CLASS_MAP + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null) +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            listClass = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    spClassItems = new SpClassItems();
                    spClassItems._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                    spClassItems._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.NAME));
                    spClassItems._standard = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STANDARD));
                    listClass.add(spClassItems);
                }
                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    initSpClass(listClass);
                    break;
                case Config.ERROR:
                    Toast.makeText(AssignmentCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(AssignmentCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AsyncGetSubjects extends AsyncTask<String, Void, String> {

        SpSubjectItems spSubjectItems;
        String _requestURL;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            listSubjects = new ArrayList<>();
            _requestURL = Config.URL + Config.URL_GET_SUBJECTS_LIST + "?ClassId=";
        }

        @Override
        protected String doInBackground(String... params) {
            String _response = Config.ERROR;
            try {
                _requestURL += params[0];
                _response = dbControl.sendGetRequest(_requestURL);

                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    spSubjectItems = new SpSubjectItems();
                    spSubjectItems._name = jsonObject.getString("SubjectName");
                    spSubjectItems._id = jsonObject.getString(Config.ID);
                    listSubjects.add(spSubjectItems);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initSpSubjects(listSubjects);
            } else {
                Toast.makeText(AssignmentCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterSPClass extends ArrayAdapter<SpClassItems> {

        LayoutInflater inflater;
        ViewHolder holder;

        public AdapterSPClass(@NonNull Context context, @LayoutRes int resource, @NonNull List<SpClassItems> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                holder.tvItem.setGravity(Gravity.LEFT);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        private class ViewHolder {
            TextView tvItem;
        }
    }

    private void initSpStandard(ArrayList<SpClassItems> list, String _Standard) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<SpClassItems> listByStand = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (spClassItems._standard.equals(_Standard)) {
                    listByStand.add(spClassItems);
                }
            }
            if ((listByStand != null) && (listByStand.size() > 0))
                spStandard.setAdapter(null);
            spStandard.setAdapter(new AdapterSPClass(AssignmentCreateActivity.this, android.R.layout.simple_spinner_dropdown_item, listByStand));
        }
    }

    private void initSpClass(ArrayList<SpClassItems> list) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listStandard = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (!listStandard.contains(spClassItems._standard)) {
                    listStandard.add(spClassItems._standard);
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AssignmentCreateActivity.this, R.layout.sp_item, listStandard);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spClass.setAdapter(adapter);
        }else {
            Toast.makeText(this,"Classes not mapped",Toast.LENGTH_SHORT).show();
        }
    }

    private void initSpSubjects(ArrayList<SpSubjectItems> list) {
        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listSubjects = new ArrayList<>();
            for (SpSubjectItems spSubjectItems : list) {
                listSubjects.add(spSubjectItems._name);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AssignmentCreateActivity.this, R.layout.sp_item, listSubjects);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spSubjects.setAdapter(adapter);
        }else {
            Toast.makeText(this,"Subjects not mapped",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncClassList.cancel(true);
        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(AssignmentCreateActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(AssignmentCreateActivity.this, NotificationService.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncClassList.cancel(true);
        }catch (Exception e){

        }
    }
}
