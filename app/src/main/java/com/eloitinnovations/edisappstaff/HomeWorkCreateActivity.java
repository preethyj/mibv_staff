package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeWorkCreateActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ImageButton btnHome;
    private TextView tvTitle;
    private ProgressBar pbTitle;

    private Spinner spClass, spStandard, spSubjects;
    private EditText etWork, etSMSContent;
    private Button btnSubmit;
    private CheckBox cbNeedSMS;

    private ArrayList<SpClassItems> listClass = new ArrayList<>();
    private ArrayList<SpSubjectItems> listSubjects = new ArrayList<>();
    private SharedPreferences spLogin;
    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();

    private String _class_id;
    private String _subject_id;
    private String _staff_id;
    private String _academic_year_id;

    private AsyncClassList asyncClassList;
    private AsyncApplyHomeWork asyncApplyHomeWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_create);
        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        pbTitle = (ProgressBar) findViewById(R.id.pbFragmentSub);
        btnHome.setVisibility(View.GONE);

        tvTitle.setText("Create Homework");
        //pbTitle.setVisibility(View.VISIBLE);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(HomeWorkCreateActivity.this, HomeActivity.class));
            }
        });
        spClass = (Spinner) findViewById(R.id.spHWCClass);
        spStandard = (Spinner) findViewById(R.id.spHWCStandard);
        spSubjects = (Spinner) findViewById(R.id.spHWCSubjects);
        btnSubmit = (Button) findViewById(R.id.btnHWCSubmit);
        etWork = (EditText) findViewById(R.id.etHWCWork);
        etSMSContent = (EditText) findViewById(R.id.etHWCSMSContent);
        cbNeedSMS = (CheckBox) findViewById(R.id.cbHWCNeedSMS);

        cbNeedSMS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etSMSContent.setVisibility(View.VISIBLE);
                } else {
                    etSMSContent.setVisibility(View.GONE);
                }
            }
        });
        spSubjects.setOnItemSelectedListener(this);
        spClass.setOnItemSelectedListener(this);
        spStandard.setOnItemSelectedListener(this);


        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncClassList.cancel(true);
        asyncClassList = new AsyncClassList();
        asyncClassList.execute();
        if(((listClass != null) && (listClass.size() > 0))&&((listSubjects != null) && (listSubjects.size() > 0)))
        {
            if((etWork.getText().toString().isEmpty()))
            {

                btnSubmit.setClickable(false);
            }else
            {
                btnSubmit.setClickable(true);
            }
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((listClass != null) && (listClass.size() > 0))&&((listSubjects != null) && (listSubjects.size() > 0))) {
                    if(etWork.getText().toString().isEmpty()) {
                        etWork.setError("Work can't be empty");
                    }
                    else
                    {
                        new AsyncApplyHomeWork().execute();
                        onBackPressed();
                    }
                }else {
                    Toast.makeText(HomeWorkCreateActivity.this, "Homework can't be created without class and subject mapping", Toast.LENGTH_SHORT).show();
                }
            }
        });
        try {

            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_HW_Creat_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == spStandard.getId()) {
            String _standard = (String) parent.getItemAtPosition(position);
            initSpClass(listClass, _standard);
        } else if (parent.getId() == spClass.getId()) {
            SpClassItems spClassItems = (SpClassItems) parent.getItemAtPosition(position);
            _class_id = spClassItems._id;
            new AsyncGetSubjects().execute(spClassItems._id);
        } else if (parent.getId() == spSubjects.getId()) {
            String _subject_name = (String) parent.getItemAtPosition(position);
            for (SpSubjectItems items : listSubjects) {
                if (items._name.equals(_subject_name))
                    _subject_id = items._id;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class SpSubjectItems {
        String _id;
        String _name;
    }

    private class SpClassItems {
        String _name;
        String _id;
        String _standard;
    }

    private class AsyncApplyHomeWork extends AsyncTask<Void, Void, String> {

        boolean needSms;
        String _work;
        String _smsContent;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _work = etWork.getText().toString();
            needSms = cbNeedSMS.isChecked();
            if (needSms) {
                _smsContent = etSMSContent.getText().toString();
            } else {
                _smsContent = "";
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            return dbControl.createHomeWork(_class_id, _subject_id, _staff_id, _smsContent, _work, _academic_year_id, String.valueOf(needSms));
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                Toast.makeText(HomeWorkCreateActivity.this, "Homework Created Successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(HomeWorkCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AsyncClassList extends AsyncTask<Void, Void, String> {

        String _requestURL;
        SpClassItems spClassItems;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _requestURL = Config.URL + Config.URL_GET_STAFF_CLASS_MAP + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null) +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
            listClass = new ArrayList<>();
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    spClassItems = new SpClassItems();
                    spClassItems._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                    spClassItems._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.NAME));
                    spClassItems._standard = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STANDARD));
                    listClass.add(spClassItems);
                }
                _response = Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case Config.SUCCESS:
                    initSpStandard(listClass);
                    break;
                case Config.ERROR:
                    Toast.makeText(HomeWorkCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(HomeWorkCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AsyncGetSubjects extends AsyncTask<String, Void, String> {

        SpSubjectItems spSubjectItems;
        String _requestURL;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            listSubjects = new ArrayList<>();
            _requestURL = Config.URL + Config.URL_GET_SUBJECTS_LIST + "?ClassId=";
        }

        @Override
        protected String doInBackground(String... params) {
            String _response = Config.ERROR;
            try {
                _requestURL += params[0];
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    spSubjectItems = new SpSubjectItems();
                    spSubjectItems._name = jsonObject.getString("SubjectName");
                    spSubjectItems._id = jsonObject.getString(Config.ID);
                    listSubjects.add(spSubjectItems);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initSpSubjects(listSubjects);
            } else {
                Toast.makeText(HomeWorkCreateActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterSPClass extends ArrayAdapter<SpClassItems> {

        LayoutInflater inflater;
        ViewHolder holder;

        public AdapterSPClass(@NonNull Context context, @LayoutRes int resource, @NonNull List<SpClassItems> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                holder.tvItem.setGravity(Gravity.LEFT);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                holder = new ViewHolder();
                holder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(holder);
            }

            holder = (ViewHolder) convertView.getTag();
            SpClassItems spClassItems = getItem(position);
            holder.tvItem.setText(spClassItems._name);
            return convertView;
        }

        private class ViewHolder {
            TextView tvItem;
        }
    }

    private void initSpClass(ArrayList<SpClassItems> list, String _Standard) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<SpClassItems> listByStand = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (spClassItems._standard.equals(_Standard)) {
                    listByStand.add(spClassItems);
                }
            }
            if ((listByStand != null) && (listByStand.size() > 0))
                spClass.setAdapter(null);
            spClass.setAdapter(new AdapterSPClass(HomeWorkCreateActivity.this, android.R.layout.simple_spinner_dropdown_item, listByStand));
        }else
        {
            Toast.makeText(this,"Classes are not mapped",Toast.LENGTH_SHORT).show();
        }
    }

    private void initSpStandard(ArrayList<SpClassItems> list) {

        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listStandard = new ArrayList<>();
            for (SpClassItems spClassItems : list) {
                if (!listStandard.contains(spClassItems._standard)) {
                    listStandard.add(spClassItems._standard);
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeWorkCreateActivity.this, R.layout.sp_item, listStandard);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spStandard.setAdapter(adapter);
        }
    }

    private void initSpSubjects(ArrayList<SpSubjectItems> list) {
        if ((list != null) && (list.size() > 0)) {
            ArrayList<String> listSubjects = new ArrayList<>();
            for (SpSubjectItems spSubjectItems : list) {
                listSubjects.add(spSubjectItems._name);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeWorkCreateActivity.this, R.layout.sp_item, listSubjects);
            adapter.setDropDownViewResource(R.layout.sp_item);
            spSubjects.setAdapter(adapter);
        }else
        {
            Toast.makeText(this,"Subjects are not mapped",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(HomeWorkCreateActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(HomeWorkCreateActivity.this, NotificationService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncClassList != null) && (asyncClassList.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncClassList.cancel(true);
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try
        {
            if ((asyncApplyHomeWork != null) && (asyncApplyHomeWork.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncApplyHomeWork.cancel(true);
        }catch (Exception e) {

        }
    }
}
