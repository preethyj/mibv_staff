package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ChatsParentMainFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private OnFragmentInteractionListener mListener;

    public ChatsParentMainFragment() {
        // Required empty public constructor
    }


    public static ChatsParentMainFragment newInstance() {
        ChatsParentMainFragment fragment = new ChatsParentMainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_parent_main, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutChatsParents);
        viewPager = (ViewPager) view.findViewById(R.id.viewpagerChatsParents);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Chats");
        btnHome.setVisibility(View.VISIBLE);
        tabLayout.removeAllTabs();
        viewPager.removeAllViews();
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        PagerChatsParents pagerChatsParents = new PagerChatsParents(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerChatsParents);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Chats");
        tabLayout.getTabAt(1).setText("Contacts");
        try {
            SharedPreferences spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN,Context.MODE_PRIVATE);
            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Cht_Prnt_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
