package com.eloitinnovations.edisappstaff;

import com.google.gson.annotations.SerializedName;

public class AnnouncementFirebaseModel {

    @SerializedName("id")
    public int id;

    @SerializedName("is_all")
    public boolean is_all;

    @SerializedName("class_ids")
    public String ClassIds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIs_all() {
        return is_all;
    }

    public void setIs_all(boolean is_all) {
        this.is_all = is_all;
    }

    public String getClassIds() {
        return ClassIds;
    }

    public void setClassIds(String classIds) {
        ClassIds = classIds;
    }
}
