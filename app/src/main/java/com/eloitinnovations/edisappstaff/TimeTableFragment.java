package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class TimeTableFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TAB_COUNT = "mTabCount";
    private static final String ARG_LIST_TIME_TABLE = "mListTimeTable";

    private LinearLayout llNothing;
    private ProgressBar pbTitle;

    // TODO: Rename and change types of parameters
    private int mTabCount;
    private ArrayList<ItemTimeTable> mListTimeTable;

    private OnFragmentInteractionListener mListener;


    public TimeTableFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TimeTableFragment newInstance(int tabCount, ArrayList<ItemTimeTable> listTimeTable) {
        TimeTableFragment fragment = new TimeTableFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TAB_COUNT, tabCount);
        args.putParcelableArrayList(ARG_LIST_TIME_TABLE,listTimeTable);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTabCount = getArguments().getInt(ARG_TAB_COUNT);
            mListTimeTable = getArguments().getParcelableArrayList(ARG_LIST_TIME_TABLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        llNothing = (LinearLayout) view.findViewById(R.id.llNothingP);
        llNothing.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pbTitle = (ProgressBar) getActivity().findViewById(R.id.pbTopBarCont);

        if((mListTimeTable!=null)&&(mListTimeTable.size()>0)){
            setListAdapter(new AdapterTimeTable(getContext(),0,mListTimeTable));
        }else{
            llNothing.setVisibility(View.VISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class AdapterTimeTable extends ArrayAdapter<ItemTimeTable>{

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterTimeTable(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemTimeTable> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                convertView = inflater.inflate(R.layout.list_time_table,null);
                viewHolder = new ViewHolder();
                viewHolder.tvPeriod = (TextView) convertView.findViewById(R.id.tvTTListPeriod);
                viewHolder.tvClass = (TextView) convertView.findViewById(R.id.tvTTListClass);
                viewHolder.tvSubject = (TextView)convertView.findViewById(R.id.tvTTListSubject);
                viewHolder.tvTime = (TextView)convertView.findViewById(R.id.tvTTListTime);
                viewHolder.tvStaff = (TextView) convertView.findViewById(R.id.tvTTListStaff);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemTimeTable timeTable = getItem(position);
            viewHolder.tvPeriod.setText(timeTable._period_name);
            viewHolder.tvSubject.setText(timeTable._subject);
            viewHolder.tvClass.setText(timeTable._class_name);
            viewHolder.tvTime.setText(timeTable._start_time+"-"+timeTable._end_time);
            viewHolder.tvStaff.setText(timeTable._staff_name);
            return convertView;
        }

        private class ViewHolder{
            TextView tvPeriod;
            TextView tvSubject;
            TextView tvClass;
            TextView tvTime;
            TextView tvStaff;
        }
    }

}
