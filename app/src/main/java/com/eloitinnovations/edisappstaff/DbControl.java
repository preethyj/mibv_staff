package com.eloitinnovations.edisappstaff;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by user on 17-05-2017.
 */

public class DbControl {

    public String sendGetRequest(String _requestURL) {
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(_requestURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String s;
            while ((s = bufferedReader.readLine()) != null) {
                sb.append(s + "\n");
            }
            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String sendLeavePost(LeaveApply leaveApply) {

        try {
            String data = URLEncoder.encode(Config.LEAVE_ID, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_leave_type(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.REASON, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_reason(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.FROM, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_from(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.TO, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_to(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.FROM_CHECKED, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_from_checked(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.TO_CHECKED, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_to_checked(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.STAFF_ID, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_staff_id(), "UTF-8");
            data += "&" + URLEncoder.encode(Config.ACADEMIC_YEAR_ID, "UTF-8") +
                    "=" + URLEncoder.encode(leaveApply.get_academic_year_id(), "UTF-8");
            URL url = new URL(Config.URL + Config.URL_STAFF_LEAVE_API_APPLY);
            URLConnection connection = url.openConnection();

            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            reader.close();
            writer.close();
            return sb.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String sendAttendance(String _post_data, String _class_id, String _academic_year_id, String _staff_id) {
        try {
            if ((_post_data != null) && (!_post_data.equals("")) && (!_post_data.isEmpty())) {
                _post_data = _post_data.substring(0, _post_data.length() - 1);
            }
            String data = URLEncoder.encode(Config.POST_DATA, "UTF-8") +
                    "=" + URLEncoder.encode(_post_data, "UTF-8");
            data += "&" + URLEncoder.encode(Config.CLASS_ID, "UTF-8") +
                    "=" + URLEncoder.encode(_class_id, "UTF-8");
            data += "&" + URLEncoder.encode(Config.ACADEMIC_YEAR_ID, "UTF-8") +
                    "=" + URLEncoder.encode(_academic_year_id, "UTF-8");
            data += "&" + URLEncoder.encode(Config.STAFF_ID, "UTF-8") +
                    "=" + URLEncoder.encode(_staff_id, "UTF-8");
            URL url = new URL(Config.URL + Config.URL_STUDENT_ATTENDANCE_MARK);
            URLConnection connection = url.openConnection();

            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if (jsonObject.getBoolean(Config.MARK_ATT)) {
                return Config.SUCCESS;
            } else {
                return Config.ERROR;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (Exception e) {
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String sendStudLeaveApprove(String id,String _status,String staffId){
        try{
            String data = URLEncoder.encode(Config.ID,"UTF-8")+
                    "=" +URLEncoder.encode(id,"UTF-8");
            data += "&" +URLEncoder.encode(Config.STATUS,"UTF-8")+
                    "="+URLEncoder.encode(_status,"UTF-8");
            data += "&" +URLEncoder.encode(Config.STAFF_ID,"UTF-8")+
                    "="+URLEncoder.encode(staffId,"UTF-8");
            URL url = new URL(Config.URL+Config.URL_APROVE_STUDENT_LEAVE_API);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!=null){
                sb.append(line);
            }
            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if(jsonObject.getString("success").equals("true")){
                return Config.SUCCESS;
            }else{
                return Config.ERROR;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        }catch (Exception e){
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String createHomeWork(String _class_id,String _subject_id,String _staff_id,String _sms_content,String _content,String _academic_year_id,String _need_sms){
        try{
            String data = URLEncoder.encode("ClassId","UTF-8")+
                    "=" +URLEncoder.encode(_class_id,"UTF-8");
            data += "&" +URLEncoder.encode("SubjectId","UTF-8")+
                    "="+URLEncoder.encode(_subject_id,"UTF-8");
            data += "&" +URLEncoder.encode("StaffId","UTF-8")+
                    "="+URLEncoder.encode(_staff_id,"UTF-8");
            data += "&" +URLEncoder.encode("SMSContent","UTF-8")+
                    "="+URLEncoder.encode(_sms_content,"UTF-8");
            data += "&" +URLEncoder.encode("Content","UTF-8")+
                    "="+URLEncoder.encode(_content,"UTF-8");
            data += "&" +URLEncoder.encode("AcademicYearId","UTF-8")+
                    "="+URLEncoder.encode(_academic_year_id,"UTF-8");
            data += "&" +URLEncoder.encode("NeedSMS","UTF-8")+
                    "="+URLEncoder.encode(_need_sms,"UTF-8");
            URL url = new URL(Config.URL+Config.URL_CREATE_HW_API);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!=null){
                sb.append(line);
            }
            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if(jsonObject.getString("status").equals("true")){
                return Config.SUCCESS;
            }else{
                return Config.ERROR;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        }catch (Exception e){
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String createAssignment(String _class_id,String _subject_id,String _staff_id,String _last_date,String _title
            ,String _description,String _max_mark,String _academic_year_id,String _need_sms){
        try{
            String data = URLEncoder.encode("ClassId","UTF-8")+
                    "=" +URLEncoder.encode(_class_id,"UTF-8");
            data += "&" +URLEncoder.encode("SubjectId","UTF-8")+
                    "="+URLEncoder.encode(_subject_id,"UTF-8");
            data += "&" +URLEncoder.encode("StaffId","UTF-8")+
                    "="+URLEncoder.encode(_staff_id,"UTF-8");
            data += "&" +URLEncoder.encode("LastDate","UTF-8")+
                    "="+URLEncoder.encode(_last_date,"UTF-8");
            data += "&" +URLEncoder.encode("Title","UTF-8")+
                    "="+URLEncoder.encode(_title,"UTF-8");
            data += "&" +URLEncoder.encode("Description","UTF-8")+
                    "="+URLEncoder.encode(_description,"UTF-8");
            data += "&" +URLEncoder.encode("MaxMark","UTF-8")+
                    "="+URLEncoder.encode(_max_mark,"UTF-8");
            data += "&" +URLEncoder.encode("AcademicYearId","UTF-8")+
                    "="+URLEncoder.encode(_academic_year_id,"UTF-8");
            data += "&" +URLEncoder.encode("NeedSMS","UTF-8")+
                    "="+URLEncoder.encode(_need_sms,"UTF-8");
            URL url = new URL(Config.URL+Config.URL_CREATE_ASSIGN_API);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!=null){
                sb.append(line);
            }
            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if(jsonObject.getString("status").equals("true")){
                return Config.SUCCESS;
            }else{
                return Config.ERROR;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        }catch (Exception e){
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String sendMessgeToStaff(String _subject,String _content,String _staffs,String _staff_id){
        try{
            String data = URLEncoder.encode("subject","UTF-8")+
                    "=" +URLEncoder.encode(_subject,"UTF-8");
            data += "&" +URLEncoder.encode("content","UTF-8")+
                    "="+URLEncoder.encode(_content,"UTF-8");
            data += "&" +URLEncoder.encode("staffs","UTF-8")+
                    "="+URLEncoder.encode(_staffs,"UTF-8");
            data += "&" +URLEncoder.encode("staffId","UTF-8")+
                    "="+URLEncoder.encode(_staff_id,"UTF-8");
           URL url = new URL(Config.URL+Config.URL_SND_STAFF_CMNCTN_MSG);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!=null){
                sb.append(line);
            }
            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if(jsonObject.getString("status").equals("true")){
                return Config.SUCCESS;
            }else{
                return Config.ERROR;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        }catch (Exception e){
            e.printStackTrace();
            return Config.ERROR;
        }
    }

    public String sendMessgeToParent(String _class_id,String _stud_id,String _staff_id,String _msg,String _academic_year_id){
        try{
            String data = URLEncoder.encode("ClassId","UTF-8")+
                    "=" +URLEncoder.encode(_class_id,"UTF-8");
            data += "&" +URLEncoder.encode("StudId","UTF-8")+
                    "="+URLEncoder.encode(_stud_id,"UTF-8");
            data += "&" +URLEncoder.encode("StaffId","UTF-8")+
                    "="+URLEncoder.encode(_staff_id,"UTF-8");
            data += "&" +URLEncoder.encode("Msg","UTF-8")+
                    "="+URLEncoder.encode(_msg,"UTF-8");
            data += "&" +URLEncoder.encode("AcademicYearId","UTF-8")+
                    "="+URLEncoder.encode(_academic_year_id,"UTF-8");
            URL url = new URL(Config.URL+Config.URL_SEND_PARENT_MSG);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!=null){
                sb.append(line);
            }
            reader.close();
            writer.close();

            JSONObject jsonObject = new JSONObject(sb.toString());
            if(jsonObject.getString("status").equals("true")){
                return Config.SUCCESS;
            }else{
                return Config.ERROR;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            return Config.ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            return Config.ERROR;
        }catch (Exception e){
            e.printStackTrace();
            return Config.ERROR;
        }
    }


}
