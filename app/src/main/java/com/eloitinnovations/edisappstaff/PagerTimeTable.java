package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by user on 11-04-2017.
 */

public class PagerTimeTable extends FragmentStatePagerAdapter {

    StaffControl staffControl = new StaffControl();
    int tabCount;
    ArrayList<ItemTimeTable> listTimeTable;
    ArrayList<ItemDays> listItemDays;

    private Context mContext;
    public PagerTimeTable(FragmentManager fm, Context context, int tabCount,ArrayList<ItemDays> listDays) {
        super(fm);
        this.mContext = context;
        this.tabCount = tabCount;
        this.listItemDays = listDays;
    }

    @Override
    public Fragment getItem(int position) {
        this.listTimeTable = listItemDays.get(position).itemTimeTables;
        return TimeTableFragment.newInstance(position,this.listTimeTable);
    }

    @Override
    public int getCount() {
        return tabCount;
    }
    public View getTabView(int position){
        View view = LayoutInflater.from(mContext).inflate(R.layout.tablayout_time_table,null);
        TextView tvCurr;
        tvCurr = (TextView) view.findViewById(R.id.tvTimeTableTabHCur);
        tvCurr.setText(staffControl.getDayByInt(position));
        tvCurr.setAlpha((float) 0.45);
        return view;
    }


}
