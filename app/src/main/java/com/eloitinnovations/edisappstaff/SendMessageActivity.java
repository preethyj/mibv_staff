package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SendMessageActivity extends AppCompatActivity implements View.OnClickListener {

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;

    private String _intent_name;
    private String _intent_id;
    private String _staff_id;
    private String _subject;
    private String _class_id,_stud_id,_academic_year_id;

    private Toolbar toolbar;
    private ProgressBar pbTitle;
    private TextView tvTitle;
    private ImageButton btnHome;
    private ListView lvHistoryList;
    private EditText etMessage;
    private Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        pbTitle = (ProgressBar) toolbar.findViewById(R.id.pbTopBarCont);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);

        _intent_name = getIntent().getStringExtra(Config.INTENT_NAME);
        _intent_id = getIntent().getStringExtra(Config.INTENT_ID);
        _class_id = getIntent().getStringExtra(Config.INTENT_CLASS_ID);
        _stud_id = getIntent().getStringExtra(Config.INTENT_STUD_ID);

        tvTitle.setText(_intent_name);
        pbTitle.setVisibility(View.GONE);
        btnHome.setOnClickListener(this);

        lvHistoryList = (ListView) findViewById(R.id.lvSMHistoryList);
        etMessage = (EditText) findViewById(R.id.etSMMessage);
        btnSend = (Button) findViewById(R.id.btnSMSend);
        btnSend.setOnClickListener(this);

        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID,null);

        new AsyncGetMessages().execute(_intent_id);

    }

    @Override
    public void onClick(View v) {
        if (v == btnHome) {
            finish();
            startActivity(new Intent(SendMessageActivity.this, HomeActivity.class));
        } else if (v == btnSend) {
            new AsyncSendmessage().execute("'" + _intent_id + "'");
        }
    }

    private class ItemMessages {
        String _type;
        String _id;
        String _msg;
        String _date;
    }

    private class AsyncGetMessages extends AsyncTask<String, Void, String> {

        ArrayList<ItemMessages> list;
        ItemMessages itemMessages;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
               // System.out.println(Config.URL + Config.URL_MESSAGE_DETAILS + "?MsgId=" + _intent_id);
                JSONObject jsonObject = new JSONObject(dbControl.sendGetRequest(Config.URL + Config.URL_MESSAGE_DETAILS + "?MsgId=" + _intent_id));
                JSONArray jsonArrayStaff = jsonObject.getJSONArray("Notification");
                list = new ArrayList<>();
                for (int i = jsonArrayStaff.length() - 1; i >= 0; i--) {
                    if(jsonArrayStaff.getJSONObject(i).getString("Id").equals(_intent_id)) {
                        itemMessages = new ItemMessages();
                        itemMessages._id = jsonArrayStaff.getJSONObject(i).getString("Id");
                        itemMessages._msg = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("Message"));
                        String _date = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("UpdateDate"));
                        Calendar calendar = staffControl.getDateByString(_date);
                        itemMessages._date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                        itemMessages._type = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("Name"));
                        list.add(itemMessages);
                    }
                }
                return Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                return Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                if ((list != null) && (list.size() > 0)) {
                    lvHistoryList.setAdapter(new AdapterSendMessages(SendMessageActivity.this, 0, list));
                    lvHistoryList.post(new Runnable() {
                        @Override
                        public void run() {

                            lvHistoryList.setSelection(lvHistoryList.getCount() - 1);
                        }
                    });
                } else {


                }
            } else {
                if (_intent_id != "0")
                    Toast.makeText(SendMessageActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterSendMessages extends ArrayAdapter<ItemMessages> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterSendMessages(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemMessages> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            ItemMessages itemMessages = getItem(position);
            if (convertView == null) {
                switch (itemMessages._type) {
                    case "You":
                        viewHolder = new ViewHolder();
                        convertView = inflater.inflate(R.layout.item_message_send, null);
                        viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvSmsItemSndMsg);
                        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSmsItemSndDate);
                        convertView.setTag(viewHolder);
                        break;
                    default:
                        viewHolder = new ViewHolder();
                        convertView = inflater.inflate(R.layout.item_message_recieve, null);
                        viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvSmsItemRecMsg);
                        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSmsItemRecDate);
                        convertView.setTag(viewHolder);
                        break;

                }

            }
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.tvMsg.setText(itemMessages._msg);
            viewHolder.tvDate.setText(itemMessages._date);
            return convertView;
        }

        private class ViewHolder {
            TextView tvMsg;
            TextView tvDate;
        }
    }


    private class AsyncSendmessage extends AsyncTask<String, Void, String> {

        String _msg;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _msg = etMessage.getText().toString();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                   return dbControl.sendMessgeToParent(_class_id,_stud_id,_staff_id,_msg,_academic_year_id);
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                Toast.makeText(SendMessageActivity.this, "Message Send Successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(SendMessageActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(SendMessageActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(SendMessageActivity.this, NotificationService.class));
    }
}
