package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class HomeworkFragment extends Fragment {


    private StaffControl staffControl = new StaffControl();
    private DbControl dbControl = new DbControl();
    private ListView lvHWList;
    private SharedPreferences spLogin;

    private LinearLayout llNothingP;
    private ProgressBar pbTitle;
    private Button btnCreate;

    private String _staff_id;
    private String _academic_year_id;

    private AsyncGetHomeWorks asyncGetHomeWorks;

    private OnFragmentInteractionListener mListener;

    public HomeworkFragment() {
        // Required empty public constructor
    }


    public static HomeworkFragment newInstance() {
        HomeworkFragment fragment = new HomeworkFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_homework, container, false);
        lvHWList = (ListView) view.findViewById(R.id.lvHWList);
        btnCreate = (Button) view.findViewById(R.id.btnHWCreate);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Homework");
        btnHome.setVisibility(View.VISIBLE);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HomeWorkCreateActivity.class));
            }
        });

        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        if ((asyncGetHomeWorks != null) && (asyncGetHomeWorks.getStatus().equals(AsyncTask.Status.RUNNING)))
            asyncGetHomeWorks.cancel(true);
        asyncGetHomeWorks = new AsyncGetHomeWorks();
        asyncGetHomeWorks.execute();
        try {

            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_HW_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ItemHomeWork {
        String _work_id;
        String _class_name;
        String _subject;
        String _work;
        String _day;
        String _month;
        String _year;

    }

    private class AsyncGetHomeWorks extends AsyncTask<Void, Void, String> {

        String _requestURL;
        ArrayList<ItemHomeWork> list;
        ItemHomeWork itemHomeWork;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
            _requestURL = Config.URL + Config.URL_HOME_WORK_VIEW_API + "?" + Config.STAFF_ID + "=" + _staff_id +
                    "&" + Config.ACADEMIC_YEAR_ID + "=" + _academic_year_id;
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response = Config.ERROR;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    itemHomeWork = new ItemHomeWork();
                    itemHomeWork._work_id = staffControl.getNullStr(jsonObject.getString("WorkId"));
                    itemHomeWork._work = staffControl.getNullStr(jsonObject.getString("Work"));
                    itemHomeWork._class_name = staffControl.getNullStr(jsonObject.getString("ClassName"));
                    itemHomeWork._subject = staffControl.getNullStr(jsonObject.getString("Subject"));
                    String _date = staffControl.getNullStr(jsonObject.getString("date"));
                    itemHomeWork._day = _date.substring(0, _date.indexOf("/"));
                    itemHomeWork._month = staffControl.getMonthById(Integer.valueOf(_date.substring(_date.indexOf("/") + 1, _date.lastIndexOf("/"))) - 1);
                    itemHomeWork._year = _date.substring(_date.lastIndexOf("/") + 1, _date.length());
                    list.add(itemHomeWork);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {

            if (s.equals(Config.SUCCESS)) {
                initList(list);
            } else {
                Toast.makeText(getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterHWList extends ArrayAdapter<ItemHomeWork> implements View.OnClickListener {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterHWList(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemHomeWork> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_home_work, null);
                viewHolder = new ViewHolder();
                viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tvHWListDay);
                viewHolder.tvMonth = (TextView) convertView.findViewById(R.id.tvHWListMonth);
                viewHolder.tvYear = (TextView) convertView.findViewById(R.id.tvHWListYear);
                viewHolder.tvSubject = (TextView) convertView.findViewById(R.id.tvHWListSubject);
                viewHolder.tvClass = (TextView) convertView.findViewById(R.id.tvHWListClass);
                viewHolder.tvWork = (TextView) convertView.findViewById(R.id.tvHWListWork);
                viewHolder.btnViewMore = (Button) convertView.findViewById(R.id.btnHWListViewMore);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            ItemHomeWork itemHomeWork = getItem(position);
            viewHolder.tvDay.setText(itemHomeWork._day);
            viewHolder.tvMonth.setText(itemHomeWork._month);
            viewHolder.tvYear.setText(itemHomeWork._year);
            viewHolder.tvSubject.setText(itemHomeWork._subject);
            viewHolder.tvClass.setText(itemHomeWork._class_name);
            String _work = "";
            if ((!itemHomeWork._work.isEmpty()) && (itemHomeWork._work.length() > 45)) {
                _work = itemHomeWork._work.substring(0, 45) + " ...";
            } else {
                _work = itemHomeWork._work;
            }
            viewHolder.tvWork.setText(_work);
            viewHolder.position = position;
            viewHolder.btnViewMore.setTag(viewHolder);
            viewHolder.btnViewMore.setOnClickListener(this);
            return convertView;

        }

        @Override
        public void onClick(View v) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            ItemHomeWork itemHomeWork = getItem(viewHolder.position);
            if (v == viewHolder.btnViewMore) {
                if (viewHolder.btnViewMore.getText().equals("View More")) {
                    viewHolder.btnViewMore.setText("View Less");
                    viewHolder.tvWork.setText(itemHomeWork._work);
                } else {
                    viewHolder.btnViewMore.setText("View More");
                    String _work = "";
                    if ((!itemHomeWork._work.isEmpty()) && (itemHomeWork._work.length() > 45))
                        _work = itemHomeWork._work.substring(0, 45) + " ...";
                    viewHolder.tvWork.setText(_work);
                }
            }
        }

        private class ViewHolder {
            TextView tvDay;
            TextView tvMonth;
            TextView tvYear;
            TextView tvSubject;
            TextView tvClass;
            TextView tvWork;
            Button btnViewMore;
            int position;
        }
    }

    private void initList(ArrayList<ItemHomeWork> list) {
        if ((list != null) && (list.size() > 0)) {
            lvHWList.setAdapter(new AdapterHWList(getContext(), 0, list));
            llNothingP.setVisibility(View.GONE);
        }else{
            llNothingP.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncGetHomeWorks != null) && (asyncGetHomeWorks.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetHomeWorks.cancel(true);
        } catch (Exception e) {

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            asyncGetHomeWorks = new AsyncGetHomeWorks();
            asyncGetHomeWorks.execute();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

