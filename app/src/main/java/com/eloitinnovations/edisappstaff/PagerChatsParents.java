package com.eloitinnovations.edisappstaff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by user on 21-04-2017.
 */

public class PagerChatsParents extends FragmentStatePagerAdapter {

    private int tabCount;

    public PagerChatsParents(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new ContactsParentsFragment();
            case 0:
            default:
                return new ChatsParentFragment();
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}
