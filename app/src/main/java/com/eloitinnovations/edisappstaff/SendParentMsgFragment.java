package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SendParentMsgFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_NAME = "NAME";
    private static final String ARG_ID = "ID";
    private static final String ARG_CLASS_ID = "CLASS_ID";
    private static final String ARG_STUD_ID = "STUD_ID";

    // TODO: Rename and change types of parameters
    private String mNAME;
    private String mID;
    private String mCLASS_ID;
    private String mSTUD_ID;

    private OnFragmentInteractionListener mListener;

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;


    private String _subject;
    private String _academic_year_id, _staff_id;

    private ProgressBar pbTitle;
    private TextView tvTitle;
 //   private ListView lvHistoryList;
    private EditText etMessage;
    private Button btnSend;
    private RecyclerView rvmy_recycler_view;
    private RecyclerView.Adapter rcAdapter;
    private RecyclerView.LayoutManager rcLayoutManager;

    private AsyncGetMessages asyncGetMessages;

    public SendParentMsgFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SendParentMsgFragment newInstance(String NAME, String ID, String CLASS_ID, String STUD_ID) {
        SendParentMsgFragment fragment = new SendParentMsgFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, NAME);
        args.putString(ARG_ID, ID);
        args.putString(ARG_CLASS_ID, CLASS_ID);
        args.putString(ARG_STUD_ID, STUD_ID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNAME = getArguments().getString(ARG_NAME);
            mID = getArguments().getString(ARG_ID);
            mCLASS_ID = getArguments().getString(ARG_CLASS_ID);
            mSTUD_ID = getArguments().getString(ARG_STUD_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_parent_msg, container, false);

        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
     //   lvHistoryList = (ListView) view.findViewById(R.id.lvSMHistoryList);
        etMessage = (EditText) view.findViewById(R.id.etSMMessage);
        btnSend = (Button) view.findViewById(R.id.btnSMSend);
        rvmy_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        tvTitle.setText(mNAME);
        pbTitle.setVisibility(View.GONE);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMessage.getText().toString().isEmpty())
                    etMessage.setError("Message is empty");
                else
                    new AsyncSendmessage().execute("'" + mID + "'");
            }
        });

        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        try {
            if ((asyncGetMessages != null) && (asyncGetMessages.getStatus().equals(true)))
                asyncGetMessages.cancel(true);
            asyncGetMessages = new AsyncGetMessages();
            asyncGetMessages.execute(mID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if ((asyncGetMessages != null) && (asyncGetMessages.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncGetMessages.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ItemMessages {
        String _type;
        String _id;
        String _msg;
        String _date;
    }

    private class AsyncGetMessages extends AsyncTask<String, Void, String> {

        ArrayList<ItemMessages> list;
        ItemMessages itemMessages;

        @Override
        protected void onPreExecute() {
        //    lvHistoryList.setAdapter(null);
           // lvHistoryList.removeAllViews();
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                System.out.println(Config.URL + Config.URL_MESSAGE_DETAILS + "?MsgId=" + mID);
                JSONObject jsonObject = new JSONObject(dbControl.sendGetRequest(Config.URL + Config.URL_MESSAGE_DETAILS + "?MsgId=" + mID));
                JSONArray jsonArrayStaff = jsonObject.getJSONArray("Notification");
                list = new ArrayList<>();
                for (int i = 0; i < jsonArrayStaff.length(); i++) {
                    itemMessages = new ItemMessages();
                    itemMessages._id = jsonArrayStaff.getJSONObject(i).getString("Id");
                    itemMessages._msg = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("Message"));
                    String _date = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("UpdateDate"));
                    Calendar calendar = staffControl.getDateByString(_date);
                    itemMessages._date = String.valueOf(calendar.get(Calendar.DATE)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
                    itemMessages._type = staffControl.getNullStr(jsonArrayStaff.getJSONObject(i).getString("Name")).equals("You")?"You":"He";
                    list.add(itemMessages);
                }
                return Config.SUCCESS;

            } catch (JSONException e) {
                e.printStackTrace();
                return Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                if ((list != null) && (list.size() > 0)) {
                    rvmy_recycler_view.setHasFixedSize(true);
                    rcLayoutManager = new LinearLayoutManager(getContext());
                    rvmy_recycler_view.setLayoutManager(rcLayoutManager);
                    rcAdapter = new RCCustomAdapter(list);
                    rvmy_recycler_view.setAdapter(rcAdapter);
                    rvmy_recycler_view.post(new Runnable() {
                        @Override
                        public void run() {
                            rvmy_recycler_view.scrollToPosition(list.size()-1);
                        }
                    });

       //             lvHistoryList.setAdapter(new AdapterSendMessages(getActivity(), 0, list));
          /*          lvHistoryList.post(new Runnable() {
                        @Override
                        public void run() {
                            lvHistoryList.setSelection(lvHistoryList.getCount() - 1);
                        }
                    });
                */
                } else {


                }
            } else {
                if (mID != "0")
                    Toast.makeText(getActivity(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterSendMessages extends ArrayAdapter<ItemMessages> {

        LayoutInflater inflater;
        ViewHolder viewHolder;

        public AdapterSendMessages(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemMessages> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            ItemMessages itemMessages = getItem(position);
            if (convertView == null) {
                switch (itemMessages._type) {
                    case "He":
                        viewHolder = new ViewHolder();
                        convertView = inflater.inflate(R.layout.item_message_send, null);
                        viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvSmsItemSndMsg);
                        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSmsItemSndDate);
                        try
                        {
                            viewHolder.tvMsg.setMovementMethod(LinkMovementMethod.getInstance());
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        convertView.setTag(viewHolder);
                        break;
                    case "You":
                        viewHolder = new ViewHolder();
                        convertView = inflater.inflate(R.layout.item_message_recieve, null);
                        viewHolder.tvMsg = (TextView) convertView.findViewById(R.id.tvSmsItemRecMsg);
                        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSmsItemRecDate);
                        try
                        {
                            viewHolder.tvMsg.setMovementMethod(LinkMovementMethod.getInstance());
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        convertView.setTag(viewHolder);
                        break;
                }
            }
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.tvMsg.setText(itemMessages._msg);
            viewHolder.tvDate.setText(itemMessages._date);
            return convertView;
        }

        private class ViewHolder {
            TextView tvMsg;
            TextView tvDate;
        }
    }


    private class AsyncSendmessage extends AsyncTask<String, Void, String> {

        String _msg;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            _msg = etMessage.getText().toString();
            btnSend.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                return dbControl.sendMessgeToParent(mCLASS_ID, mSTUD_ID, _staff_id, _msg, _academic_year_id);
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                pbTitle.setVisibility(View.GONE);
                btnSend.setEnabled(true);
            }catch (Exception e){
                e.printStackTrace();
            }
            if (s.equals(Config.SUCCESS)) {
                try{
                    etMessage.setText("");
                    if((asyncGetMessages!=null)&&(asyncGetMessages.getStatus().equals(Status.RUNNING)))
                        asyncGetMessages.cancel(true);
                    asyncGetMessages = new AsyncGetMessages();
                    asyncGetMessages.execute(mID);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Toast.makeText(getActivity(), "Message Send Successfully", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            } else {
                Toast.makeText(getActivity(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class RCCustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        private ArrayList<ItemMessages> listItemMessages;

        public class ViewHolder extends RecyclerView.ViewHolder{

            public View view;
            public ViewHolder(View itemView) {
                super(itemView);
                view = itemView;
            }
        }

        public class ViewHolderRec extends RecyclerView.ViewHolder{

            TextView tvMsg,tvDate;
            public ViewHolderRec(View itemView) {
                super(itemView);
                tvMsg = (TextView) itemView.findViewById(R.id.tvSmsItemRecMsg);
                tvDate = (TextView) itemView.findViewById(R.id.tvSmsItemRecDate);
            }
        }

        public class ViewHolderSnd extends RecyclerView.ViewHolder{

            TextView tvMsg,tvDate;
            public ViewHolderSnd(View itemView) {
                super(itemView);
                tvMsg = (TextView) itemView.findViewById(R.id.tvSmsItemSndMsg);
                tvDate = (TextView) itemView.findViewById(R.id.tvSmsItemSndDate);
            }
        }

        @Override
        public int getItemViewType(int position) {
            //return super.getItemViewType(position);
            return (listItemMessages.get(position)._type.equals("You")?1:2);
        }

        public RCCustomAdapter(ArrayList<ItemMessages> listItemMessages){
            this.listItemMessages = listItemMessages;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType){
                case 1:
                    View viewSnd = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_send,parent,false);
                    return new ViewHolderSnd(viewSnd);
                case 2:
                    View viewRec = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_recieve,parent,false);
                    return new ViewHolderRec(viewRec);
                default:
                    return null;
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (holder.getItemViewType()){
                case 1:
                    ViewHolderSnd viewHolderSnd = (ViewHolderSnd)holder;
                    viewHolderSnd.tvMsg.setText(listItemMessages.get(position)._msg);
                    viewHolderSnd.tvDate.setText(listItemMessages.get(position)._date);
                    break;
                case 2:
                    ViewHolderRec viewHolderRec = (ViewHolderRec) holder;
                    viewHolderRec.tvMsg.setText(listItemMessages.get(position)._msg);
                    viewHolderRec.tvDate.setText(listItemMessages.get(position)._date);
                    break;
                default:
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return listItemMessages.size();
        }

    }
}
