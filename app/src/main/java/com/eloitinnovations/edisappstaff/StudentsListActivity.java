package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class StudentsListActivity extends AppCompatActivity {

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;
    private String strClass;

    private Toolbar toolbar;
    private TextView tvTitle;
    private ProgressBar pbTitle;
    private ImageButton btnHome;
    private ListView lvStudents;
    private AsyncGetStudents asyncGetStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);
        toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);
        pbTitle = (ProgressBar) toolbar.findViewById(R.id.pbTopBarCont);
        lvStudents = (ListView) findViewById(R.id.lvStudentList);
        pbTitle.setVisibility(View.GONE);

        Intent intent = getIntent();
        String className = intent.getExtras().getString("ClassName");

        tvTitle.setText("Students List - " + className);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spLogin = getSharedPreferences(Config.SHR_LOGIN, MODE_PRIVATE);
        asyncGetStudents = new AsyncGetStudents();
        asyncGetStudents.execute(spLogin.getString(Config.CLASS_ID, null),
                spLogin.getString(Config.ACADEMIC_YEAR_ID, null), spLogin.getString(Config.ID, null));
        try {

            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Stdnts_List_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ItemStudents {
        String _roll_no;
        String _name;
        String _admn_no;
        String _ph_no;

    }

    private class AsyncGetStudents extends AsyncTask<String, Void, String> {

        ArrayList<ItemStudents> list;
        ItemStudents itemStudents;

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                JSONArray jsonArray = new JSONArray(dbControl.sendGetRequest(Config.URL + Config.URL_GET_STDNT_DETAIL_BY_CLASS
                        + "?ClassId=" + params[0] + "&AcademicYearId=" + params[1] + "&StaffId=" + params[2]));
                // System.out.println("--->request URL student list "+request_URL);
                for (int i = 0; i < jsonArray.length(); i++) {
                    itemStudents = new ItemStudents();
                    itemStudents._roll_no = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("Rollno"));
                    itemStudents._name = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("StudentName"));
                    itemStudents._ph_no = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("PhoneNo"));
                    itemStudents._admn_no = staffControl.getNullStr(jsonArray.getJSONObject(i).getString("AdmissionNo"));
                    list.add(itemStudents);
                }
                return Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                return Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return Config.ERROR;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                if ((list != null) && (list.size() > 0)) {
                    lvStudents.setAdapter(new AdapterStudents(StudentsListActivity.this, 0, list));
                }

            } else {
                Toast.makeText(StudentsListActivity.this, Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            pbTitle.setVisibility(View.GONE);
        }
    }

    private class AdapterStudents extends ArrayAdapter<ItemStudents> {

        LayoutInflater inflater;
        ViewHolder viewhHlder;

        public AdapterStudents(@NonNull Context context, @LayoutRes int resource, @NonNull List<ItemStudents> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_student_list, null);
                viewhHlder = new ViewHolder();
                viewhHlder.tvAdmNo = (TextView) convertView.findViewById(R.id.tvSLAAdmnNo);
                viewhHlder.tvName = (TextView) convertView.findViewById(R.id.tvSLAName);
                viewhHlder.tvPhNo = (TextView) convertView.findViewById(R.id.tvSLAPhNo);
                viewhHlder.tvRollNo = (TextView) convertView.findViewById(R.id.tvSLARollNo);
                convertView.setTag(viewhHlder);
            }

            viewhHlder = (ViewHolder) convertView.getTag();
            ItemStudents itemStudents = getItem(position);
            viewhHlder.tvAdmNo.setText(itemStudents._admn_no);
            viewhHlder.tvRollNo.setText(itemStudents._roll_no);
            viewhHlder.tvPhNo.setText(itemStudents._ph_no);
            viewhHlder.tvName.setText(itemStudents._name);
            return convertView;
        }

        private class ViewHolder {
            TextView tvRollNo, tvName, tvPhNo, tvAdmNo;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (asyncGetStudents != null) {
                if ((asyncGetStudents != null) && (asyncGetStudents.getStatus().equals(AsyncTask.Status.RUNNING))) {
                    asyncGetStudents.cancel(true);
                }
            }
        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}
