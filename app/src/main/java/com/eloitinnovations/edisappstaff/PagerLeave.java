package com.eloitinnovations.edisappstaff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by user on 08-04-2017.
 */

public class PagerLeave extends FragmentStatePagerAdapter {

    int tabCount;
    public PagerLeave(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                TabStaffLeave tabStaffLeave = new TabStaffLeave();
                return tabStaffLeave;
            case 1:
                TabStudentLeave tabStudentLeave = new TabStudentLeave();
                return tabStudentLeave;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
