package com.eloitinnovations.edisappstaff;

/**
 * Created by user on 13-05-2017.
 */

public class LeaveApply {
    private String _reason;
    private String _leave_type;
    private String _from;
    private String _to;
    private String _from_checked;
    private String _to_checked;
    private String _staff_id;
    private String _academic_year_id;


    public LeaveApply(String _reason, String _leave_type, String _from, String _to, String _from_checked, String _to_checked, String _staff_id, String _academic_year_id) {
        this._reason = _reason;
        this._leave_type = _leave_type;
        this._from = _from;
        this._to = _to;
        this._from_checked = _from_checked;
        this._to_checked = _to_checked;
        this._staff_id = _staff_id;
        this._academic_year_id = _academic_year_id;
    }


    public String get_reason() {
        return _reason;
    }

    public String get_leave_type() {
        return _leave_type;
    }

    public String get_from() {
        return _from;
    }

    public String get_to() {
        return _to;
    }

    public String get_from_checked() {
        return _from_checked;
    }

    public String get_to_checked() {
        return _to_checked;
    }

    public String get_staff_id() {
        return _staff_id;
    }

    public String get_academic_year_id() {
        return _academic_year_id;
    }
}
