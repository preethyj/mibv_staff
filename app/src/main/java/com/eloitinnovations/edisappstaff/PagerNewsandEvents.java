package com.eloitinnovations.edisappstaff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by user on 10-02-2017.
 */

public class PagerNewsandEvents extends FragmentStatePagerAdapter {

    int tabCount;
    public PagerNewsandEvents(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                TabNewsFragment tabNewsFragment = new TabNewsFragment();
                return tabNewsFragment;
            case 1:
                TabEventsFragment tabEventsFragment = new TabEventsFragment();
                return tabEventsFragment;
            default:
                return null;
        }
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return tabCount;
    }
}
