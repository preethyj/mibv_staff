package com.eloitinnovations.edisappstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

public class StaffViewMessageActivity extends AppCompatActivity {

    private TextView tvToTv, tvTo, tvSubject, tvMsg, tvDate;
    private TextView tvTitle;
    private ImageButton btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_view_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbSubActivity);
        btnHome = (ImageButton) toolbar.findViewById(R.id.btnHome);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        tvTitle.setText("Message");
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvToTv = (TextView) findViewById(R.id.tvSMVTOtv);
        tvTo = (TextView) findViewById(R.id.tvSMVTo);
        tvSubject = (TextView) findViewById(R.id.tvSMVSubject);
        tvMsg = (TextView) findViewById(R.id.tvSMVMessage);
        tvDate = (TextView) findViewById(R.id.tvSMVDate);
        try {
            tvToTv.setText(getIntent().getStringExtra("TO_TV"));
            tvTo.setText(getIntent().getStringExtra("TO"));
            tvSubject.setText(getIntent().getStringExtra("SUBJECT"));
            tvMsg.setText(getIntent().getStringExtra("MSG"));
            tvDate.setText(getIntent().getStringExtra("DATE"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(StaffViewMessageActivity.this, NotificationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        startService(new Intent(StaffViewMessageActivity.this, NotificationService.class));
    }
}
