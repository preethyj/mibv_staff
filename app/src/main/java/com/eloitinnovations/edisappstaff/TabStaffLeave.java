package com.eloitinnovations.edisappstaff;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TabStaffLeave extends ListFragment implements AdapterView.OnItemSelectedListener {
    private OnFragmentInteractionListener mListener;

    private DbControl dbControl = new DbControl();
    private StaffControl staffControl = new StaffControl();
    private SharedPreferences spLogin;

    private SimpleDateFormat simpleDateFormat;
    private DatePickerDialog dialogFromDate;
    private DatePickerDialog dialogToDate;

    private LinearLayout llNothingP;
    private TextView tvNothingP;
    private ProgressBar pbLoading;
    private FloatingActionButton fbLeaveApply;
    private Dialog dialogLeaveApply;
    private EditText etDialogReason;
    private EditText etFromDate;
    private EditText etTodate;
    private CheckBox cbFromHaflfDay;
    private CheckBox cbToHalfDate;
    private Spinner spLeaveType;
    private Button btnSubmit;
    private ProgressBar pbDialogLeaveApply;

    private String _leave_type_id;
    private String _leave_reason;
    private String _leave_from;
    private String _leave_to;
    private String _leave_from_checked;
    private String _leave_to_checked;
    private String _staff_id;
    private String _academic_year_id;

    private AsyncGetLeaves asyncGetLeaves;
    private int Success=0;

    public TabStaffLeave() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TabStaffLeave newInstance(String param1, String param2) {
        TabStaffLeave fragment = new TabStaffLeave();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_staff_leave, container, false);
        fbLeaveApply = (FloatingActionButton) view.findViewById(R.id.fabDialogStaffLeaveApply);
        pbLoading = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        llNothingP = (LinearLayout) view.findViewById(R.id.llNothingP);
        tvNothingP = (TextView) view.findViewById(R.id.tvNothingHasp);
        tvNothingP.setText("No leave history yet.");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        pbLoading.setVisibility(View.INVISIBLE);

        _staff_id = spLogin.getString(Config.ID, null);
        _academic_year_id = spLogin.getString(Config.ACADEMIC_YEAR_ID, null);

        dialogLeaveApply = new Dialog(getActivity());
        dialogLeaveApply.requestWindowFeature(Window.FEATURE_NO_TITLE & Window.FEATURE_ACTION_BAR_OVERLAY);

        fbLeaveApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dialogLeaveApply.setContentView(R.layout.dialog_staff_leave_apply);
                etDialogReason = (EditText) dialogLeaveApply.findViewById(R.id.etDialogLeaveApplyReason);
                etFromDate = (EditText) dialogLeaveApply.findViewById(R.id.etDialogStaffLeaveFromDate);
                etTodate = (EditText) dialogLeaveApply.findViewById(R.id.etDialogStaffLeaveToDate);
                cbFromHaflfDay = (CheckBox) dialogLeaveApply.findViewById(R.id.cbDialogStaffLeaveFromHalfDay);
                cbToHalfDate = (CheckBox) dialogLeaveApply.findViewById(R.id.cbDialogStaffLeaveToHalfDay);
                btnSubmit = (Button) dialogLeaveApply.findViewById(R.id.btnDialogStaffLeaveSubmit);
                spLeaveType = (Spinner) dialogLeaveApply.findViewById(R.id.spLeaveType);
                pbDialogLeaveApply = (ProgressBar) dialogLeaveApply.findViewById(R.id.pbDialogLeaveApply);
                etFromDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogFromDate.show();
                    }
                });
                etTodate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogToDate.show();
                    }
                });

                simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
                Calendar calendar = Calendar.getInstance();
                dialogFromDate = new DatePickerDialog(TabStaffLeave.this.getContext(), R.style.DateDialog, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        etFromDate.setText(simpleDateFormat.format(newDate.getTime()));
                    }

                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialogToDate = new DatePickerDialog(TabStaffLeave.this.getContext(), R.style.DateDialog, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        etTodate.setText(simpleDateFormat.format(newDate.getTime()));
                    }

                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


                new AsyncTask<Void, Void, String>() {

                    ArrayList<RowLeaveTypes> listLeaveTypes;
                    String _requestURL;
                    RowLeaveTypes rowLeaveTypes;

                    @Override
                    protected void onPreExecute() {
                        pbDialogLeaveApply.setVisibility(View.VISIBLE);
                        listLeaveTypes = new ArrayList<>();
                        _requestURL = Config.URL + Config.URL_GET_STAFF_LEAVE_TYPES + Config.STAFF_ID
                                + "=" + spLogin.getString(Config.ID, null) + "&AcademicYearId=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
                    }

                    @Override
                    protected String doInBackground(Void... voids) {
                        String _response;
                        try {
                            _response = dbControl.sendGetRequest(_requestURL);
                            JSONArray jsonArray = new JSONArray(_response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                rowLeaveTypes = new RowLeaveTypes();
                                rowLeaveTypes._id = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.ID));
                                rowLeaveTypes._type = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.LEAVE_TYPE));
                                listLeaveTypes.add(rowLeaveTypes);
                            }
                            _response = Config.SUCCESS;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            _response = Config.ERROR;
                        } catch (Exception e) {
                            _response = Config.ERROR;
                        }
                        return _response;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        try {
                            if (s.equals(Config.SUCCESS)) {
                                if((listLeaveTypes != null) && (listLeaveTypes.size() > 0)) {
                                    spLeaveType.setAdapter(new AdapterSpinnerLeave(getContext(), R.layout.sp_item, listLeaveTypes));
                                    spLeaveType.setOnItemSelectedListener(TabStaffLeave.this);
                                }
                                else
                                {
                                    Toast.makeText(getContext(),"Leave Types not mapped",Toast.LENGTH_SHORT).show();
                                }
                            }
                            pbDialogLeaveApply.setVisibility(View.GONE);
                        } catch (Exception e) {

                        }
                    }
                }.execute();

                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_leave_type_id != null) {
                            if (etDialogReason.getText().toString().isEmpty()) {
                                etDialogReason.setError("Reason can't be empty");
                            } else if (etFromDate.getText().toString().isEmpty()) {
                                etFromDate.setError("From Date can't be empty");
                            } else if (etTodate.getText().toString().isEmpty()) {
                                etTodate.setError("To Date can't be empty");
                            } else {
                                btnSubmit.setClickable(false);
                                _leave_reason = etDialogReason.getText().toString();
                                _leave_from = etFromDate.getText().toString();
                                _leave_to = etTodate.getText().toString();
                                _leave_from_checked = (cbFromHaflfDay.isChecked() ? "true" : "false");
                                _leave_to_checked = (cbToHalfDate.isChecked() ? "true" : "false");
                                LeaveApply leaveApply = new LeaveApply(_leave_reason, _leave_type_id,
                                        _leave_from, _leave_to, _leave_from_checked, _leave_to_checked, _staff_id, _academic_year_id);
                                new AsyncTask<LeaveApply, String, String>() {
                                    @Override
                                    protected void onPreExecute() {
                                        pbDialogLeaveApply.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    protected String doInBackground(LeaveApply... leaveApplies) {
                                        try {
                                            String _response = dbControl.sendLeavePost(leaveApplies[0]);
                                            JSONObject jsonObject = new JSONObject(_response);
                                            return jsonObject.getString(Config.MESSAGE);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            return Config.ERROR;
                                        }
                                    }

                                    @Override
                                    protected void onPostExecute(String s) {
                                        if (!s.equals(Config.ERROR)) {
                                            Toast.makeText(TabStaffLeave.this.getContext(), s, Toast.LENGTH_SHORT).show();
                                            dialogLeaveApply.dismiss();
                                            Success=1;
                                        } else {
                                            Toast.makeText(TabStaffLeave.this.getContext(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                                        }
                                        pbDialogLeaveApply.setVisibility(View.GONE);
                                    }
                                }.execute(leaveApply);
                                if(Success==1) {
                                    try {
                                        asyncGetLeaves = new AsyncGetLeaves();
                                        asyncGetLeaves.execute();
                                    } catch (Exception e) {}
                                }
                            }
                        }
                        else
                        {
                            Toast.makeText(getContext(),"Leave Types can't be empty",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                if (dialogLeaveApply.isShowing()) {
                    dialogLeaveApply.dismiss();
                }
                dialogLeaveApply.show();
            }
        });
        new AsyncGetLeaves().execute();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        RowLeaveTypes leaveTypes = (RowLeaveTypes) parent.getItemAtPosition(position);
        _leave_type_id = leaveTypes._id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private class Leave  {
        String _leave_type;
        String _day;
        String _month;
        String _date;
        String _status;
        String _days;
        String _reason;
        String _office_remark;
        String _from_half;
        String _to_half;
    }

    private class RowLeaveTypes {
        String _id;
        String _type;
    }


    private void initLeaveAdapter(ArrayList<Leave> list) {
        if ((list != null) && (list.size() > 0)) {
            setListAdapter(new LeaveAdapter(TabStaffLeave.this.getContext(), 0, list));
        } else {
            llNothingP.setVisibility(View.VISIBLE);
        }
    }


    private class AdapterSpinnerLeave extends ArrayAdapter<RowLeaveTypes> {

        LayoutInflater inflater;
        ViewHolder viewHolder;


        public AdapterSpinnerLeave(@NonNull Context context, @LayoutRes int resource, @NonNull List<RowLeaveTypes> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                viewHolder = new ViewHolder();
                viewHolder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            RowLeaveTypes rowLeaveTypes = getItem(position);
            viewHolder.tvItem.setText(rowLeaveTypes._type);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.sp_item_leave, null);
                viewHolder = new ViewHolder();
                viewHolder.tvItem = (TextView) convertView.findViewById(R.id.tvSPItemLeave);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            RowLeaveTypes rowLeaveTypes = getItem(position);
            viewHolder.tvItem.setText(rowLeaveTypes._type);
            return convertView;
        }

        private class ViewHolder {
            TextView tvItem;
        }
    }

    private class AsyncGetLeaves extends AsyncTask<Void, Void, String> {

        ArrayList<Leave> list;
        String _requestURL;
        Leave leave;

        @Override
        protected void onPreExecute() {
            pbLoading.setVisibility(View.VISIBLE);
            list = new ArrayList<>();
            _requestURL = Config.URL + Config.URL_STAFF_LEAVES_API + Config.STAFF_ID + "=" + spLogin.getString(Config.ID, null)
                    + "&" + Config.ACADEMIC_YEAR_ID + "=" + spLogin.getString(Config.ACADEMIC_YEAR_ID, null);
        }

        @Override
        protected String doInBackground(Void... params) {
            String _response;
            try {
                _response = dbControl.sendGetRequest(_requestURL);
                JSONArray jsonArray = new JSONArray(_response);
                for (int i = jsonArray.length()-1; i>=0; i--) {
                    leave = new Leave();
                    leave._leave_type = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.LEAVE_TYPE));
                    String _from = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.FROM));
                    String _to = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.TO));
                    String _applied_date = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.REQUESTED_DATE));
                    leave._day = String.valueOf(_applied_date.substring(0, _applied_date.indexOf("/")));
                    leave._month = staffControl.getMonthById(Integer.valueOf(_applied_date.substring(_applied_date.indexOf("/") + 1, _applied_date.lastIndexOf("/"))) - 1);

                    leave._status = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.STATUS));
                    leave._days = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.DAYS));
                    leave._reason = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.REASON));
                    leave._office_remark = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.OFFICE_REMARK));
                    leave._from_half = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.FROM_HALF));
                    leave._to_half = staffControl.getNullStr(jsonArray.getJSONObject(i).getString(Config.TO_HALF));
                    leave._date = String.valueOf(staffControl.getDateByString(_from).get(Calendar.DATE)) + "/"
                            + String.valueOf(staffControl.getDateByString(_from).get(Calendar.MONTH) + 1) + "/"
                            + String.valueOf(staffControl.getDateByString(_from).get(Calendar.YEAR)) + " - "
                            + String.valueOf(staffControl.getDateByString(_to).get(Calendar.DATE)) + "/"
                            + String.valueOf(staffControl.getDateByString(_to).get(Calendar.MONTH) + 1) + "/"
                            + String.valueOf(staffControl.getDateByString(_to).get(Calendar.YEAR));
                    list.add(leave);
                }
                _response = Config.SUCCESS;
            } catch (JSONException e) {
                e.printStackTrace();
                _response = Config.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                _response = Config.ERROR;
            }
            return _response;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals(Config.SUCCESS)) {
                initLeaveAdapter(list);
            }
            pbLoading.setVisibility(View.GONE);
        }
    }

    private class LeaveAdapter extends ArrayAdapter<Leave> {

        ViewHolder viewHolder;
        LayoutInflater inflater;

        public LeaveAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Leave> objects) {
            super(context, resource, objects);
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_staff_leave, null);
                viewHolder = new ViewHolder();
                viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tvSLListDay);
                viewHolder.tvDuration = (TextView) convertView.findViewById(R.id.tvSLListDuration);
                viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSLListDate);
                viewHolder.tvMonth = (TextView) convertView.findViewById(R.id.tvSLListMonth);
                viewHolder.tvOfficeRemark = (TextView) convertView.findViewById(R.id.tvSLListOfficeRemark);
                viewHolder.tvReason = (TextView) convertView.findViewById(R.id.tvSLListReason);
                viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvSLListStatus);
                viewHolder.tvType = (TextView) convertView.findViewById(R.id.tvSLListLeaveType);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            Leave leave = getItem(position);
            viewHolder.tvType.setText(leave._leave_type);
            viewHolder.tvStatus.setText(leave._status);
            viewHolder.tvReason.setText(leave._reason);
            viewHolder.tvOfficeRemark.setText(leave._office_remark);
            viewHolder.tvDate.setText(leave._date);
            viewHolder.tvDay.setText(leave._day);
            viewHolder.tvDuration.setText(leave._days);
            viewHolder.tvMonth.setText(leave._month);
            return convertView;
        }

        private class ViewHolder {
            TextView tvType;
            TextView tvDate;
            TextView tvStatus;
            TextView tvDuration;
            TextView tvDay;
            TextView tvMonth;
            TextView tvReason;
            TextView tvOfficeRemark;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try
        {
            asyncGetLeaves=new AsyncGetLeaves();
            asyncGetLeaves.execute();
        }catch (Exception e)
        {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if ((asyncGetLeaves != null) && (asyncGetLeaves.getStatus() == AsyncTask.Status.RUNNING)) {
                asyncGetLeaves.cancel(true);
            }
        }catch (Exception e){

        }
    }
}
