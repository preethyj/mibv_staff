package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private SharedPreferences spLogin;
    private TextView tvEmail, tvApnmntDate, tvCnfrmtnDate, tvDOB, tvCntctNmbr, tvCrntAddr, tvPrmntAddr, tvEmrgncyNmbr,tvClass;
    private LinearLayout llClassDetails;
    private String _class_name;
    private Button btnStudentList;

    public ProfileFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        btnStudentList = (Button) view.findViewById(R.id.btnProfileStudentList);
        tvApnmntDate = (TextView) view.findViewById(R.id.tvProfileAppntmntDate);
        tvCnfrmtnDate = (TextView) view.findViewById(R.id.tvProfileCnfrmtnDate);
        tvCntctNmbr = (TextView) view.findViewById(R.id.tvProfileCntctNmbr);
        tvCrntAddr = (TextView) view.findViewById(R.id.tvProfileCrntAddr);
        tvDOB = (TextView) view.findViewById(R.id.tvProfileDOB);
        tvEmail = (TextView) view.findViewById(R.id.tvProfileEmail);
        tvEmrgncyNmbr = (TextView) view.findViewById(R.id.tvProfileEmrgncyNmbr);
        tvPrmntAddr = (TextView) view.findViewById(R.id.tvProfilePrmntAddr);
        tvClass = (TextView) view.findViewById(R.id.tvProfileClass);
        llClassDetails = (LinearLayout) view.findViewById(R.id.llClassDetails);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        ImageButton btnHome = (ImageButton) getActivity().findViewById(R.id.btnActionBarMainHome);
        tvTitle.setText("Profile");
        btnHome.setVisibility(View.VISIBLE);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN, Context.MODE_PRIVATE);
        tvPrmntAddr.setText(spLogin.getString(Config.ADDRESS, null));
        tvEmrgncyNmbr.setText(spLogin.getString(Config.EMERGENCY_CONTACT, null));
        tvDOB.setText(spLogin.getString(Config.DATE_OF_BIRTH, null));
        tvEmail.setText(spLogin.getString(Config.EMAIL, null));
        tvApnmntDate.setText(spLogin.getString(Config.APPOINMENT_DATE, null));
        tvCnfrmtnDate.setText(spLogin.getString(Config.CONFIRMATION_DATE, null));
        tvCntctNmbr.setText(spLogin.getString(Config.MOBILE, null));
        tvCrntAddr.setText(spLogin.getString(Config.CURRENT_ADDRESS, null));
        String _class_id = spLogin.getString(Config.CLASS_ID, null);
        _class_name = spLogin.getString(Config.CLASS_NAME,null);
        try {
            if ((_class_id == null) || (_class_id.isEmpty()) || (_class_id.equals("")) || (_class_id.equals("0"))) {
                llClassDetails.setVisibility(View.GONE);
            } else {
                llClassDetails.setVisibility(View.VISIBLE);
                tvClass.setText(StaffControl.getNullStr(spLogin.getString(Config.CLASS_NAME,null)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnStudentList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), StudentsListActivity.class);
                i.putExtra("ClassName",_class_name);
                startActivity(i);
            }
        });
        try {
            AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("STAFF_" + getResources().getString(R.string.app_name).substring(0, 15) + "_Profile_" + spLogin.getString(Config.ID, null));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
