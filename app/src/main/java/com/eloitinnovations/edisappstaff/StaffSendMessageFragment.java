package com.eloitinnovations.edisappstaff;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;


public class StaffSendMessageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TO_STAFF_ID = "TO_STAFF_ID";
    private static final String ARG_STAFF_NAME = "STAFF_NAME";
    private static final String ARG_SUBJECT = "SUBJECT";

    private TextView tvDate,tvTo;
    private ProgressBar pbTitle;
    private EditText etSubject,etMsg;
    private Button btnSend;
    private TextView tvTitle;
    private SharedPreferences spLogin;
    private AsyncSendMsg asyncSendMsg;
    private DbControl dbControl = new DbControl();

    // TODO: Rename and change types of parameters
    private String mSUBJECT;
    private String mTO_STAFF_ID;
    private String mSTAFF_NAME;

    private OnFragmentInteractionListener mListener;

    public StaffSendMessageFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static StaffSendMessageFragment newInstance(String TO_STAFF_ID, String STAFF_NAME,String SUBJECT) {
        StaffSendMessageFragment fragment = new StaffSendMessageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TO_STAFF_ID, TO_STAFF_ID);
        args.putString(ARG_STAFF_NAME, STAFF_NAME);
        args.putString(ARG_SUBJECT,SUBJECT);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTO_STAFF_ID = getArguments().getString(ARG_TO_STAFF_ID);
            mSTAFF_NAME = getArguments().getString(ARG_STAFF_NAME);
            mSUBJECT = getArguments().getString(ARG_SUBJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_staff_send_message, container, false);
        tvTo = (TextView)view.findViewById(R.id.tvStaffSendTo);
        tvDate= (TextView) view.findViewById(R.id.tvStaffSendDate);
        etSubject =(EditText) view.findViewById(R.id.etStaffSendSubject);
        etMsg = (EditText) view.findViewById(R.id.etStaffSendMessage);
        btnSend = (Button) view.findViewById(R.id.btnStaffSendSend);
        pbTitle = (ProgressBar) view.findViewById(R.id.pbFragmentSub);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle = (TextView) getActivity().findViewById(R.id.tvActionBarMainTitle);
        tvTitle.setText("Message");
        etSubject.setText(mSUBJECT);
        spLogin = getActivity().getSharedPreferences(Config.SHR_LOGIN,Context.MODE_PRIVATE);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //              try{
                if(etSubject.getText().toString().isEmpty())
                    etSubject.setError("Subject is empty.");
                else if(etMsg.getText().toString().isEmpty())
                    etMsg.setError("Message is empty");
                else {
                    if ((asyncSendMsg != null) && (asyncSendMsg.getStatus().equals(AsyncTask.Status.RUNNING)))
                        asyncSendMsg.cancel(true);
                    asyncSendMsg = new AsyncSendMsg();
                    asyncSendMsg.execute(etSubject.getText().toString(), etMsg.getText().toString(), mTO_STAFF_ID, spLogin.getString(Config.ID, null));
                    //            }catch (Exception e){
                    //              e.printStackTrace();
                    //            Toast.makeText(StaffSendActivity.this, "Error in fetching Data", Toast.LENGTH_SHORT).show();
                    //      }

                }

            }
        });
        try{
            if(!mTO_STAFF_ID.isEmpty()&&!mSTAFF_NAME.isEmpty()){
                Calendar calendar = Calendar.getInstance();
                tvDate.setText((String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)+"/"+(calendar.get(Calendar.MONTH)+1)
                        +"/"+calendar.get(Calendar.YEAR))));
                tvTo.setText(mSTAFF_NAME);
            }else{
                getActivity().onBackPressed();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private class AsyncSendMsg extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            pbTitle.setVisibility(View.VISIBLE);
            btnSend.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... params) {
            //  System.out.println("sss "+params[0]+" "+params[1]+" "+params[2]+" "+params[3]);
            return dbControl.sendMessgeToStaff(params[0],params[1],params[2],params[3]);
            //   return Config.SUCCESS;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals(Config.SUCCESS)){
                Toast.makeText(getActivity(), "Message Sent Successfully.", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            }else{
                Toast.makeText(getActivity(), Config.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
            btnSend.setEnabled(true);
            pbTitle.setVisibility(View.GONE);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if((asyncSendMsg!=null)&&(asyncSendMsg.getStatus().equals(AsyncTask.Status.RUNNING)))
                asyncSendMsg.cancel(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
